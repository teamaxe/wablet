/*
 *  mesh.cpp
 *  springstructure
 *
 *  Created by Robert Tubb on 07/06/2011.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#define _USE_MATH_DEFINES
#include <cmath>
#include "mesh.h"
//#include "testApp.h"
#include "lump.h"


//--------------------------------------------------------------
Mesh::Mesh()
{
	// construct an empty structure
	numSprings = 0;
	numLumps = 0;
    
    scanPath = new ScanPath();

	syrup = false;
	GRAB_RANGE = 0.040;
    propagationSpeed = 0.5; // this will be changed by adjusting k/m
    std::cout << "constructed mesh base" << std::endl;
    
    m = 123.0;
    k = 0.1;
    f = 0.999;

    prevLump = -1;
}


Mesh::~Mesh(){
    std::cout << "destroying mesh" << std::endl;
	if(avLumpPos != nullptr)
		delete[] avLumpPos;
	delete [] lumps; 
	delete [] springs;
    delete scanPath;
}
///////////
// interface (called from testApp)
///////////


//--------------------------------------------------------------
void Mesh::update()
{
	// update all the springs and masses sequentially
    // INTERP HACK - this bit of code is to debug pops and clicks
  /*  
    static int updown = 0;
    updown++;
    if(updown > 24){
        for(int i=0;i<numLumps;i++){
            lumps[i].position.y = 0.4;
        }	
    
    }else{
        
        for(int i=0;i<numLumps;i++){
            lumps[i].position.y = 0.6;
        }	
     
    }
    if(updown > 48) updown = 0;
  	// update the ends of the springs to reflect lump movement
	
	for(int i=0;i<numSprings;i++){
		springs[i].updateEndPoints();
	}	  
    
    return;
    
   */
    forces.update();
	// calculate all the forces in the springs
	for(int i=0;i<numSprings;i++){
		springs[i].calculateForce();
	}	
    
	// accelerate the lumps
	for(int i=0;i<numLumps;i++){
		lumps[i].applyForce (forces);
	}	
    
    // move filters into apply force? not really, they're not forces.
    // apply homing filter
	for(int i=0;i<numLumps;i++){
		lumps[i].homingFilter(forces.homingAmt);
	}	    
    // apply averaging filter
    // 
    averagingFilter(forces.avFilterAmt);
    
	// update the ends of the springs to reflect lump movement
	for(int i=0;i<numSprings;i++){
		springs[i].updateEndPoints();
	}	
    
    // scan the updated mesh
    scanPath->updateWavetables();
}
//--------------------------------------------------------------
void Mesh::averagingFilter(double amt){
    // we need a temporary store for average position, can't update in place with central diffs
    if (avLumpPos == nullptr)
		avLumpPos = new TwoVector[numLumps];
    
    for(int i=0;i<numLumps;i++){
		avLumpPos[i] = lumps[i].averageOfConnected();
	}	 
    // now we can set pos
    for(int i=0;i<numLumps;i++){
            // mix in the average with the 'real'

            lumps[i].position.x = (1 - amt)*lumps[i].position.x + amt*avLumpPos[i].x;
            lumps[i].position.y = (1 - amt)*lumps[i].position.y + amt*avLumpPos[i].y;

	}    
    
    
}
//--------------------------------------------------------------
void Mesh::draw(){
	// draw all the components belonging to mesh

	int i;
	for(i=0;i<numSprings;i++){
		springs[i].draw();
	}

	for(i=0;i<numLumps;i++){
		lumps[i].draw();
	}

    // scanPath->draw(); // uncomment if you want to see the output waveform


}

void Mesh::paint (Graphics& g, int x, int y)
{
    int i;
    for(i=0;i<numSprings;i++){
        springs[i].paint (g, x, y);
    }
    
    for(i=0;i<numLumps;i++){
        lumps[i].paint (g, x, y);
    }
}
//--------------------------------------------------------------
TwoVector Mesh::calculateCentre(){
    // calculates average of all lump positions
    
    double x = 0, y = 0;
    for(int i = 0; i < numLumps; i++){
        x += lumps[i].position.x;
        y += lumps[i].position.y;
        
    }
    x = x/numLumps;
    y = y/numLumps;
    
    //cout << "centre : " << x << "," << y << endl;
    
    centre.setCoord(x,y);
    
    return centre;
}
//--------------------------------------------------------------
void Mesh::resetAll(){
	// it's this simple
	resetPositions();
    resetVelocities();
    clearScanPath();
    makeDefaultScanPath();
    constrain(0.0,0.0,CONSTRAIN_EDGES);
    constrain(0.0,0.0,CONSTRAIN_CORNERS);
    
	// unless we want to reset velocities...?
}
//--------------------------------------------------------------
void Mesh::resetPositions(){
	// it's this simple
	setLumpPositions();
	// unless we want to reset velocities...?
}
//--------------------------------------------------------------
void Mesh::resetVelocities(){
    for(int i = 0; i < numLumps; i++){
        lumps[i].setVelocity(0.0,0.0);
        
    }    
}
void Mesh::toggleHome(bool on){
    if(!home && on){
        home = on;
        prev = forces.homingAmt;
        forces.homingAmt = 0.3;
        

    }else if (home && !on){
        home = on; // ie false
        forces.homingAmt = prev;
    }
}
//--------------------------------------------------------------
void Mesh::resetEverything(){

}

//--------------
void Mesh::toggleSyrup(){
    if(syrup){
        toggleSyrup(false);
    }else{
        toggleSyrup(true);
    }
}
//--------------
void Mesh::toggleSyrup(bool on){
	if (!on){
		syrup = false;
		for(int i=0;i<numLumps;i++) lumps[i].setFriction(0.998);
	}else{
		syrup = true;
		for(int i=0;i<numLumps;i++) lumps[i].setFriction(0.90);
	}
}
//----------------------------------------------------------------
void Mesh::toggleGravity()
{
    forces.gravityOn = !forces.gravityOn;
}
//----------------------------------------------------------------
void Mesh::toggleGravity(bool on){
    if (!on)
    {
        forces.gravityOn = true;
	}else
    {
        forces.gravityOn = false;
	}
}

//----------------------------------------------------------------
void Mesh::toggleSpringForces(){

		Spring::forcesOn = !Spring::forcesOn;

}
//----------------------------------------------------------------
void Mesh::toggleSpringForces(bool on){
	if (!on){
		Spring::forcesOn = false;

	}else{
		Spring::forcesOn = true;

	}
}

//--------------------------------------------------------------
void Mesh::unconstrain(){
	
	for(int i = 0; i<numLumps; i++){
		
		lumps[i].unconstrain();
		
	}
}
//--------------------------------------------------------------
void Mesh::unconstrain(double ax, double ay, constrainMode aMode){
	int i = 0;
    TwoVector diff;
	// check if lump is within grab range
	switch (aMode) {
		case CONSTRAIN_GRAB_REGION:
			for(i = 0; i<numLumps; i++){
				
				diff.setCoord(lumps[i].position.x - ax, lumps[i].position.y - ay);
				
				if (diff.norm() < GRAB_RANGE){
					lumps[i].unconstrain();
				}
			}
			break;
		case CONSTRAIN_EDGES:
            // hmm
			break;
			
		default:
			break;
	}
}
//--------------------------------------------------------------
void Mesh::setRestLength(){
	for(int i = 0; i<numSprings; i++){
		springs[i].setRestLength();
	}

}
//--------------------------------------------------------------
void Mesh::setZeroAudioLine(){
	// set zero rest pos to where they are now
    for(int i = 0; i<numLumps; i++){
		
		lumps[i].setZeroRefPos();
		
	}
}
//--------------------------------------------------------------
void Mesh::setPropagationSpeed(double aSpeed){
    
    propagationSpeed = aSpeed;
    for(int i = 0; i < numLumps; i++){
        lumps[i].setInvMass(propagationSpeed*propagationSpeed); // inverse mass is c^2
    }
}
//--------------------------------------------------------------
void Mesh::increasePropagationSpeed(){
    
    propagationSpeed = propagationSpeed + 0.01;
    for(int i = 0; i < numLumps; i++){
        lumps[i].setInvMass(propagationSpeed*propagationSpeed); // inverse mass is c^2
    }
    std::cout << "prop speed  = " << propagationSpeed << std::endl;
}
//--------------------------------------------------------------
void Mesh::decreasePropagationSpeed(){
    
    propagationSpeed = propagationSpeed - 0.01;
    for(int i = 0; i < numLumps; i++){
        lumps[i].setInvMass(propagationSpeed*propagationSpeed); // inverse mass is c^2
    }
    std::cout << "prop speed  = " << propagationSpeed << std::endl;
}
//--------------------------------------------------------------
void Mesh::setSpringConstant(double aK){
    k = aK;
    if(sqrt(k/m) > 0.9){
        k = m * 0.9;
    }
    std::cout << "k/m = " << k/m << std::endl;
    for(int i = 0; i < numSprings; i++){
        springs[i].setSpringConstant(k); // inverse mass is c^2
    }      
}
//--------------------------------------------------------------
void Mesh::setMass(double aM){

    m = aM;

    if(sqrt(k/m) > 0.9){
        m = k/0.9;
    }
    std::cout << "k/m = " << k/m << std::endl;
    double im = 1/aM;
    for(int i = 0; i < numLumps; i++){
        lumps[i].setInvMass(im); 
    }    
}
//--------------------------------------------------------------
void Mesh::setFriction(double newF){
    f = newF;
    for(int i = 0; i < numLumps; i++){
        lumps[i].setFriction(f); 
    }        
}
//--------------------------------------------------------------
void Mesh::setMassAsym(double aAsym){
    // use m as bass and add asym linearly across mesh. might be wierd for some meshes!
    double newm,incr;
    newm = m;
    incr = aAsym*m/(numLumps-1);
    for(int i = 0; i < numLumps; i++){
        lumps[i].setInvMass(1/newm);
        newm += incr;
    }    
}
//--------------------------------------------------------------
void Mesh::setSpringConstantAsym(double aAsym){
    // from 0 to 1
    if (aAsym > 1.0){
        aAsym = 1.0;
    }
    if (aAsym < 0.0){
        aAsym = 0.0;
    }
    double newk, incr;
    newk = k;
    incr = (1-aAsym)*k/(numSprings-1);
    for(int i = 0; i < numSprings; i++){
        springs[i].setSpringConstant(newk);
        newk -= incr;
    }    
}

//--------------------------------------------------------------
void Mesh::setFrictionAsym(double aAsym){
    // multiplying it doesn't make sense!
    double newF,incr;
    newF = f;
    incr = aAsym*f/(numLumps-1);
    for(int i = 0; i < numLumps; i++){
        lumps[i].setFriction(newF); 
        newF += incr;
    }        
}
//--------------------------------------------------------------
void Mesh::checkHover(double ax, double ay){
	TwoVector diff;
	bool found = false;
	// check if lump is within grab range
	for(int i = 0; i<numLumps; i++){
		diff.setCoord(lumps[i].position.x - ax, lumps[i].position.y - ay);
		//std::cout << "diff " << diff.x << "," << diff.y << std::endl;
		if (diff.norm() < GRAB_RANGE){
			if(!found)	{
				lumps[i].highlight();
				found = true;
			}
		}else{
			lumps[i].unhighlight();
		}
	}	
	
}

//--------------------------------------------------------------
void Mesh::grab(double ax, double ay, int touchID){
	// attempt to grab a lump ax and ay should be already normalised
	
	TwoVector diff;
    
	// find closest
    int nearest = getNearestLump(ax,ay);
    diff.setCoord(lumps[nearest].position.x - ax, lumps[nearest].position.y - ay);
    //std::cout << "diff " << diff.x << "," << diff.y << std::endl;
    if (diff.norm() < GRAB_RANGE){
        std::cout << "grabbing lump " << nearest << std::endl;
        // move this lump to mouse pos
        
        lumps[nearest].grab(touchID);
    }
}
//--------------------------------------------------------------
void Mesh::grabLump(int index, int touchID) 
{
	// attempt to grab a lump ax and ay should be already normalised
	lumps[index].grab(touchID);
}
//--------------------------------------------------------------
void Mesh::unGrab(int touchID){
	
	for(int i=0; i<numLumps;i++){
        if(lumps[i].grabID == touchID){ // only release the lumps that were grabbed by this finger
            lumps[i].unGrab();
            lumps[i].unhighlight();
        }
	}
}

//--------------------------------------------------------------
void Mesh::drag(double ax, double ay, int touchID){
	// maybe set up specific id's for grabbed lumps
	for(int i=0; i<numLumps;i++){
        if(lumps[i].grabID == touchID){// only move the lumps that were grabbed by this finger
            
            lumps[i].drag(ax,ay,touchID);
        }
	}
}

void Mesh::dragDelta(double ax, double ay, int touchID)
{
	for (int i = 0; i < numLumps; i++) {
		if (lumps[i].grabID == touchID) {// only move the lumps that were grabbed by this finger

			lumps[i].dragDelta(ax, ay, touchID);
		}
	}
}
//--------------------------------------------------------------
/* also could:
 add random velocities
 etc
 */
//////////////////////
// protected utils
////////////////////////
//---------------------------------------------------------------
void Mesh::connect(int aspringnum,int alumpnum){
	//std::cout << "connecting spring " << aspringnum << " to lump " << alumpnum << std::endl;
	// should we check valid indexes?
	lumps[alumpnum].attachSpring(&springs[aspringnum]);
	springs[aspringnum].attachLump(&lumps[alumpnum]);

}
//---------------------------------------------------------------
void Mesh::connect(int aspringnum,int alumpnum,int alumpnum2){
	// better version - just connects 2 lumps with a spring
	// should we check valid indexes?
	lumps[alumpnum].attachSpring(&springs[aspringnum]);
	springs[aspringnum].attachLump(&lumps[alumpnum]);
	lumps[alumpnum2].attachSpring(&springs[aspringnum]);
	springs[aspringnum].attachLump(&lumps[alumpnum2]);
    
}

void Mesh::zeroRestLength(){
    // sets springs rest length to zero - collapses structure
    for(int i = 0; i < numSprings; i++){
        springs[i].setRestLength(0.0);
    }
}
//--------------------------------------------------------------
int Mesh::getNearestLump(double ax, double ay){
    // not used yet
	int nearestLumpIndex = -1;
	TwoVector diff;
	double distance = 1.0;
	double minDistance = 1.0;

	for(int i = 0; i<numLumps; i++){
		diff.setCoord(lumps[i].position.x - ax, lumps[i].position.y - ay);
		distance = diff.norm();
		if(distance < minDistance && !lumps[i].constrained){ // don't grab if constrained ?? 
			minDistance = distance;
			nearestLumpIndex = i;
		}
	}	
    std::cout << "nearest lump : " << nearestLumpIndex << std::endl;
    
	return nearestLumpIndex;
}
//--------------------------------------------------------------
void Mesh::clearScanPath(){
	// in between having clicked and drawn we can't play anything
    scanPath->clear();
    prevLump = -1;
}
//--------------------------------------------------------------
void Mesh::disconnectDraw(){
    prevLump = -1;
}
//--------------------------------------------------------------
int Mesh::inscribeScanPath(double ax, double ay){
    bool foundPath = false;
    int targetLump, intermediateLump; 
    int count = 0;
    TwoVector halfWay;
    // is this ridiculously complicated for no reason?
    
    targetLump = getNearestLump(ax,ay); // target is the one closest to touch point, prev is last point already on our path
    
    while(!foundPath && count < 2048){
        count++;
        if(prevLump == -1){ // (scanPath->howManyElements() == 0){ //
            //first lump
            foundPath = true;
            prevLump = targetLump;
            //std::cout << "first lump" << std::endl;
            return targetLump;
        }
        // check if this lump connected to last lump by a spring
        Spring * connectedToPrev  = lumps[targetLump].checkConnectedTo(&lumps[prevLump]);
        
        // if not connectedToLast will be null
        if (!connectedToPrev){
            //std::cout << "setting half way" << std::endl;
            // set coords to 1/2 way and recurse
            double halfdiffx = ((ax-lumps[prevLump].position.x)*0.5);
            double halfdiffy = ((ay - lumps[prevLump].position.y)*0.5);
            if (halfdiffx < 0.0001 && halfdiffy < 0.0001){
                // not found nuffun
                //std::cout << "not found a path, quitting" << std::endl;
                return -1;
            }
            halfWay.setCoord( ax - halfdiffx , ay -  halfdiffy);
            
            
            intermediateLump = inscribeScanPath(halfWay.x,halfWay.y);
            if(intermediateLump == -1){
                //std::cout << "finding intermediate didn't work" << std::endl;
                return -1;
            }
            // now check that the found intermediate targetLump is connected to the original targetlump 
            Spring * connectedToNext  = lumps[targetLump].checkConnectedTo(&lumps[prevLump]);
            if(!connectedToNext){
                // set prevLump to the interm and try again
                prevLump = intermediateLump;
                targetLump = inscribeScanPath(ax,ay);
                if(targetLump == -1){
                    //std::cout << "joining up next one didn't work" << std::endl;
                    return -1;
                }
            }
        }else{
            //std::cout << "CONNECTED! adding an element" << std::endl;
            
            // hurrah, add the spring and lump to the scanpath in the right order...
            // get pointer to lump and spring
            
            scanPath->addElement(&lumps[targetLump], connectedToPrev);
            // and set prev lump to that one
            prevLump = targetLump;
            foundPath = true;
            return targetLump; // 
        }
        
    }

    std::cout << "reached end of inscribe func" << std::endl;

    return -1;
}

// 
void Mesh::hit(double dax, double day,int velocity, GlobalForces::excitationTypes aEType, GlobalForces::excitationShapes aEShape){
    // set filter to something low
    forces.homingAmt = 0.0;
    
    double radius = 0.1, distance; // size of lump
    double impactStrength = forces.excitationStrength*velocity/1000.0; // velocity as in note loudness
    double amt = 0.0;
    TwoVector diff;
    // get all the lumps and smack em
    int xamt = forces.exciteShapeX;
    int yamt = forces.exciteShapeY;

    switch(aEShape){
    
        case GlobalForces::SINE:
            
            // applies a sine wave to all lumps, x and y
            for(int i = 0; i < numLumps; i++){
                if(!lumps[i].constrained){
                    lumps[i].position.y = lumps[i].zeroRefPos.y + impactStrength*sin(M_PI* (2*(xamt-1) + 1) * lumps[i].position.x);
                    lumps[i].position.x = lumps[i].zeroRefPos.x + impactStrength*sin(M_PI* (2*(yamt-1) + 1) * lumps[i].position.y);
                }
            }
            
            break;
        case GlobalForces::GAUSS:
            // temporarily not a gauss...
            for(int i = 0; i < numLumps; i++){
                diff.setCoord(lumps[i].position.x - dax, lumps[i].position.y - day);
                distance = diff.norm();
                if(distance < radius){
                    amt = impactStrength*(1 + cos(M_PI*distance/radius)); // a 2d hamming.
                    lumps[i].position.y += amt;
                    
                }     
            }
            break;
        case GlobalForces::NOISE:
            // 2D noise
            //only hit the scan path lumps - 350 - 399 - this is indeed horrific
            //but the scan path is currently inaccessable
            for(int i = 350; i < 399; i++)
            {
                    lumps[i].position.x += impactStrength * randomNumber.get() * 0.02;
                    lumps[i].position.y += impactStrength * randomNumber.get() * 0.02;
                    
            }     
            break;
        default:
            break;
    }
    
    
}
void Mesh::spatialHarmonic(int aharm, int aharm2)
{
    // applies a sine wave to all lumps, x and y
    for(int i = 0; i < numLumps; i++){
        if(!lumps[i].constrained){
            lumps[i].position.y = lumps[i].zeroRefPos.y + forces.touchStrength/256.0*sin(M_PI* (2*(aharm-1) + 1) * lumps[i].position.x);
            lumps[i].position.x = lumps[i].zeroRefPos.x + forces.touchStrength/256.0*sin(M_PI* (2*(aharm2-1) + 1) * lumps[i].position.y);
        }
    }
}

void Mesh::damp()
{
    // set friction and filters to something high
    //globalForces.avFilterAmt = 0.0;
    forces.homingAmt = 0.3;
}
void Mesh::forceField(double dax,double day,double strength)
{
    
    // 
}
//--------------------------------------------------------------
double Mesh::getNextSample()
{

	return 0.0;

}

//--------------------------------------------------------------
// VIRTUAL
//--------------------------------------------------------------
void Mesh::makeDefaultScanPath(){
	// make componets creates the correct number of lumps and springs according to dimensions and mesh type
	std::cout << "!!!!Mesh base class makeDefaultScanPath\n";
}

//--------------------------------------------------------------
void Mesh::setLumpPositions(){
    // places lumps in the right positions 
	std::cout << "!!!!Mesh base class setLumpPositions\n";
}
//--------------------------------------------------------------
void Mesh::makeConnections(){
    // connects the lmps and springs. the spring needs to know which lumps are at each end
    // and lumps need to know which springs attached
    // calls connect method that does this
	std::cout << "!!!!Mesh base class make connections\n";
}
//--------------------------------------------------------------
void Mesh::makeComponents(int aDimension1, int aDimension2){
	std::cout << "!!!!Mesh base class makeComponents\n";
}
//--------------------------------------------------------------
void Mesh::constrain(double ax, double ay, constrainMode aMode){
	std::cout << "!!!!Mesh base class constrain\n";
}
//--------------------------------------------------------------


/****************************************/
//		SquareCrossMesh					*/
/****************************************/
//--------------------------------------------------------------
SquareCrossMesh::SquareCrossMesh(int height, int width): Mesh() {
	
	makeComponents(height, width);
	
	setLumpPositions();
	
	makeConnections();
	
	makeDefaultScanPath();
    
    constrain(0,0,CONSTRAIN_EDGES_XY);
    
    zeroRestLength();
    
    radial = false;
	
	std::cout << "constructing SQUARE mesh\n";
}
SquareCrossMesh::~SquareCrossMesh(){
	
	std::cout << "destructing SQUARECROSS mesh\n";
}

//--------------------------------------------------------------
// make componets for square
void SquareCrossMesh::makeComponents(int aheight, int awidth){
	height = aheight;
	width = awidth;
	numLumps = height * width;
                // horz             // vert
	numSprings = (width-1)*height + (height-1)*width + 2*(height-1)*(width-1);
	
	lumps = new Lump[numLumps];
	std::cout << "made " << numLumps << " lumps\n";
	springs = new Spring[numSprings];
	std::cout << "made " << numSprings << " springs\n";
}

//--------------------------------------------------------------
void SquareCrossMesh::setLumpPositions(){
	double vspacing = 1.0/(height-1);
	double hspacing = 1.0/(width-1);
	int i =0;
	// set positions
	for(int row = 0; row < height; row++){ // go down column
		for(int col = 0;col < width; col++){ // go along row
			lumps[i].setPosition(hspacing*col,vspacing*row);
			
			i++;
		}
	}
}
//--------------------------------------------------------------
void SquareCrossMesh::makeConnections(){
	
	// could be cleverer in terms of numbering?
	int lumpnum = 0;
	int springnum = 0;
	// attach horizontal 
	for ( int i=0;i<height;i++){
		for(int j=0;j<width-1;j++){
			connect(springnum,lumpnum);
			lumpnum++;
			connect(springnum,lumpnum);
			springnum++;
			
		}
		lumpnum++;
	}
	// attach vertical 
	lumpnum = 0;
	for ( int i=0;i<width;i++){
		for(int j=0;j<height-1;j++){
			connect(springnum,lumpnum);
			lumpnum+=width;
			connect(springnum,lumpnum);
			springnum++;
			
		}
		lumpnum -= (width*(height-1)) - 1;
	}	
	// attach diagonal
	lumpnum = 0;
	for ( int i=0;i<width-1;i++){
		for(int j=0;j<height-1;j++){
			connect(springnum,lumpnum);
			lumpnum+=(width);
			connect(springnum,lumpnum+1);
			springnum++;
			
		}
		lumpnum -= (width*(height-1)) - 1;
	}	
	// attach other diagonal
	lumpnum = 1;
	for ( int i=0;i<width-1;i++){
		for(int j=0;j<height-1;j++){
			connect(springnum,lumpnum);
			lumpnum+=(width);
			connect(springnum,lumpnum-1);
			springnum++;
			
		}
		lumpnum -= (width*(height-1)) - 1;
	}	
}

//--------------------------------------------------------------
void SquareCrossMesh::constrain(double ax, double ay, constrainMode aMode){
	
	TwoVector diff;
	int i = 0;
	
	// check if lump is within grab range
	switch (aMode) {
		case CONSTRAIN_GRAB_REGION:
			for(i = 0; i<numLumps; i++){
				
				diff.setCoord(lumps[i].position.x - ax, lumps[i].position.y - ay);
				
				if (diff.norm() < GRAB_RANGE){
					lumps[i].constrain();
				}
			}
			break;
		case CONSTRAIN_EDGES:
			i = 0;
			for(int row = 0; row < height; row++){ // go down column
				for(int col = 0;col < width; col++){ // go along row
					if(row == 0 || row == height-1 || col == 0 || col == width-1){
						lumps[i].constrain();
					}
					i++;
				}
			}	
			break;
		case CONSTRAIN_EDGES_XY:
			i = 0;
			for(int row = 0; row < height; row++){ // go down column
				for(int col = 0;col < width; col++){ // go along row
					if(row == 0 || row == height-1){
						lumps[i].constrain(Lump::CONSTRAIN_Y);
					}else if( col == 0 || col == width-1){
                        lumps[i].constrain(Lump::CONSTRAIN_X);
                    }
					i++;
				}
			}	
			break;
		case CONSTRAIN_CORNERS:
			i = 0;
			for(int row = 0; row < height; row++){ // go down column
				for(int col = 0;col < width; col++){ // go along row
					if(   (row == 0 && col == 0)
					   || (row == height-1 && col == 0) 
					   || (row == 0 && col == width-1) 
					   || (row == height-1 && col == width-1)){
						lumps[i].constrain();
					}
					i++;
				}
			}
			break;
			
		default:
			break;
	}
	
}
//--------------------------------------------------------------
//--------------------------------------------------------------
void SquareCrossMesh::makeDefaultScanPath(){
    if (height < 10 || width < 10) return; // not much point
    
    int vmarg = 5;
    int hmarg = 5;
    
    int lumpno = vmarg * width + hmarg; // top left corner
    int springno = vmarg * (width - 1) + hmarg;
    // do top horz
    for(int i = 0; i < width - 2*hmarg; i++){
        scanPath->addElement(&lumps[lumpno], &springs[springno]);
        lumpno++;
        springno++;
    }
    
    // do right vert, lumpno starts the same
                // all horz spr    // left marg rows      // top margin
    springno  = height*(width-1) + ((height-1) * (width - hmarg)) + vmarg;
    
    for(int i = 0; i < height - (2 * vmarg); i++){
        scanPath->addElement(&lumps[lumpno], &springs[springno]);
        springno++; // jump to next row
        lumpno += width;   // ditto
    }
    
    
    // do bottom horz right to left
    springno = (height - vmarg) * (width - 1) + (width - hmarg - 1);
    for(int i = 0; i < width - 2*hmarg; i++){
        scanPath->addElement(&lumps[lumpno], &springs[springno]);
        springno--; // jump to next row
        lumpno--;   // ditto
    } 
    
                 // all horz spr    // left marg rows    // top margin   
    springno  = height*(width-1) + ((height-1) * hmarg) +  height - vmarg - 1;
    for(int i = 0; i < height - 2 * vmarg; i++){
        scanPath->addElement(&lumps[lumpno], &springs[springno]);
        springno--; // jump to next row
        lumpno -= width;   // ditto
    }
    
}
//--------------------------------------------------------------


/****************************************/
//		SpiderMesh					*/
/****************************************/
SpiderMesh::SpiderMesh(int aNumSpokes,int aNumRings) : Mesh(){
	numSpokes = aNumSpokes;
	numRings = aNumRings;
    
    radial = true;
	
	makeComponents(numSpokes, numRings);
	
	setLumpPositions();
	
	makeConnections();

	makeDefaultScanPath();
    
    constrain(0.0,0.0,CONSTRAIN_EDGES);
    constrain(0.0,0.0,CONSTRAIN_CORNERS);
	
	std::cout << "constructed SPIDER mesh\n";
	
	
	
}
//--------------------------------------------------------------
void SpiderMesh::makeComponents(int aDimension1, int aDimension2){
    // make componets creates the correct number of lumps and springs according to dimensions and mesh type
	std::cout << "!!!spider makeComponents\n";
	numLumps = numSpokes * numRings + 1; // +1 cos one in the middle
	numSprings = 2 * numSpokes * numRings;
	
	lumps = new Lump[numLumps];
	std::cout << "made " << numLumps << " lumps\n";
	springs = new Spring[numSprings];
	std::cout << "made " << numSprings << " springs\n";
	
				
}
//--------------------------------------------------------------
void SpiderMesh::setLumpPositions(){
	std::cout << "!spider setLumpPositions\n";
	// here we're assuming that zero spoke is to right
	// work out sines / cosines for each spoke
	int l = 1;
	double hyplen,xpos,ypos;
	
	double * cosines = new double[numSpokes];
	double * sines = new double[numSpokes];
	double angle = 2 * M_PI / numSpokes;
	
	double ringSpacing = 1.0/(2*numRings);
	
	for(int spoke = 0; spoke < numSpokes; spoke++){
		cosines[spoke] = cos(angle*spoke);
		sines[spoke] = sin(angle*spoke);
	}
		
	lumps[0].setPosition(0.5,0.5);

	for(int ring = 0; ring < numRings; ring++){
		for(int spoke = 0; spoke < numSpokes; spoke++){
			hyplen = ringSpacing * (ring+1);
			xpos = hyplen * cosines[spoke] + 0.5;
			ypos = hyplen*sines[spoke] + 0.5;
			lumps[l].setPosition(xpos,ypos);
			l++;
			
		}
	}
	delete [] cosines;
	delete [] sines;
			
}
//--------------------------------------------------------------
void SpiderMesh::makeConnections(){
	// TODO needs to be rewritten to automatically 
	// add in lumps and springs AS we're connecting
	//using std vector
	// this is dumb
	std::cout << "!!!spider make connections\n";

	// UNLIKE a spider we're going to make rings first, to make indexing the scan path easier...

	for(int ring = 0; ring < numRings; ring++){
		for(int spoke = 0; spoke < numSpokes-1; spoke++){
			//		spring ,				lump
			connect(numSpokes*ring + spoke, ring*numSpokes + spoke + 1);
			connect(numSpokes*ring + spoke, ring*numSpokes + spoke + 2);
		}
		// remember the last one on the ring goes back to first
		//		spring ,						lump
		connect(numSpokes*ring + numSpokes - 1 , ring*numSpokes + numSpokes);
		connect(numSpokes*ring + numSpokes - 1,  ring*numSpokes + 1);
	}	
	// at which point we have used numSpokes*numRings springs
	int used = numSpokes*numRings;
	// first ring is different cos it connects to central point
	for(int spoke = 0; spoke < numSpokes; spoke++){
		connect(used,0); // inner radial on center
		connect(used,spoke+1);  // outer radial
		springs[used].setRestLength(0.0); // need tension on spokes
		used++;
	}
	// now do the rest of the radial spokes
	for(int ring = 0; ring < numRings-1; ring++){
		for(int spoke = 0; spoke < numSpokes; spoke++){
			connect(used,ring*numSpokes+1+spoke); // 
			connect(used,(ring+1)*numSpokes+1+spoke);  // going to next ring out
			springs[used].setRestLength(0.0);
			used++;
		}
	}
}

//--------------------------------------------------------------
void SpiderMesh::constrain(double ax, double ay, constrainMode aMode){
	std::cout << "!!spider constrain\n";
	
	TwoVector diff;
	int i = 0;
	
	// check if lump is within grab range
	switch (aMode) {
		case CONSTRAIN_GRAB_REGION:
			for(i = 0; i<numLumps; i++){
				
				diff.setCoord(lumps[i].position.x - ax, lumps[i].position.y - ay);
				
				if (diff.norm() < GRAB_RANGE){
					lumps[i].constrain();
				}
			}
			break;
		case CONSTRAIN_EDGES:
		{
			// constrain outer ring
			int startLump = numSpokes*(numRings-1) + 1;
			for(int l = startLump; l < startLump + numSpokes; l++){
				lumps[l].constrain();
			}
	
			break;
		}
		case CONSTRAIN_CORNERS:
			// er... constrain the centre
			lumps[0].constrain();
			
			break;
			
		default:
			break;
	}
}

//--------------------------------------------------------------

void SpiderMesh::makeDefaultScanPath(){
	
	std::cout << "!!spider makeDefaultScanPath\n";
    
    // add a ring to the path
    int ringNo = ceil(numRings/2.0);
    int springno = 0, lumpno = 0;
    for(int i = 0; i < numSpokes; i++){
        lumpno = numSpokes*ringNo + 1 + i;
        springno = numSpokes*ringNo + i;
        scanPath->addElement(&lumps[lumpno], &springs[springno]);
    }
    std::cout << "!!spider scan path created on ring " << ringNo << "\n";
     
    // add a spoke to the path
/*
    int springno = 0, lumpno = 0;
    
    int springsinrings = numSpokes*numRings;
    for(int i = 0; i < numRings-1; i++){
        lumpno = numSpokes*i; // zero, numspokes etc
        springno = springsinrings + numSpokes*i; // starts from total rings, increments by numspokes etc
        scanPath->addElement(&lumps[lumpno], &springs[springno]);
    }    
*/
 }


//--------------------------------------------------------------
/****************************************/
//		Holed Spider    Mesh            */
/****************************************/
//--------------------------------------------------------------
HoledSpiderMesh::HoledSpiderMesh(int aNumSpokes,int aNumRings) : Mesh(){
	numSpokes = aNumSpokes;
	numRings = aNumRings;
    radial = true;
	
	makeComponents(numSpokes, numRings);
	
	setLumpPositions();
	
	makeConnections();
    
	makeDefaultScanPath();
    
    constrain(0.0,0.0,CONSTRAIN_EDGES);
    constrain(0.0,0.0,CONSTRAIN_CORNERS);
	
	std::cout << "constructed SPIDER mesh\n";
}

void HoledSpiderMesh::makeComponents(int aDimension1, int aDimension2){
    // make componets creates the correct number of lumps and springs according to dimensions and mesh type
	std::cout << "HOLED spider makeComponents\n";
	numLumps = numSpokes * numRings; 
	numSprings = numSpokes * numRings + numSpokes * (numRings - 1);
	
	lumps = new Lump[numLumps];
	std::cout << "made " << numLumps << " lumps\n";
	springs = new Spring[numSprings];
	std::cout << "made " << numSprings << " springs\n";
	
    
}
//--------------------------------------------------------------
void HoledSpiderMesh::setLumpPositions(){
	std::cout << "HOLED spider setLumpPositions\n";
	// here we're assuming that zero spoke is to right
	// work out sines / cosines for each spoke
	int l = 0;
	double hyplen,xpos,ypos;
	
	double * cosines = new double[numSpokes];
	double * sines = new double[numSpokes];
	double angle = 2 * M_PI / numSpokes;
	
	double ringSpacing = 1.0/(2*numRings);
	
	for(int spoke = 0; spoke < numSpokes; spoke++){
		cosines[spoke] = cos(angle*spoke);
		sines[spoke] = sin(angle*spoke);
	}
    
	for(int ring = 0; ring < numRings; ring++){
		for(int spoke = 0; spoke < numSpokes; spoke++){
			hyplen = ringSpacing * (ring+1);
			xpos = hyplen * cosines[spoke] + 0.5;
			ypos = hyplen*sines[spoke] + 0.5;
			lumps[l].setPosition(xpos,ypos);
			l++;
			
		}
	}
	delete [] cosines;
	delete [] sines;
    
}
//--------------------------------------------------------------
void HoledSpiderMesh::makeConnections(){
	// TODO needs to be rewritten to automatically 
	// add in lumps and springs AS we're connecting
	//using std vector
	// this is dumb
	std::cout << "HOLED spider make connections\n";
    
	// UNLIKE a spider we're going to make rings first, to make indexing the scan path easier...
    
	for(int ring = 0; ring < numRings; ring++){
		for(int spoke = 0; spoke < numSpokes-1; spoke++){
			//		spring ,				lump
			connect(numSpokes*ring + spoke, ring*numSpokes + spoke + 0);
			connect(numSpokes*ring + spoke, ring*numSpokes + spoke + 1);
		}
		// remember the last one on the ring goes back to first
		//		spring ,						lump
		connect(numSpokes*ring + numSpokes - 1 , ring*numSpokes + numSpokes - 1);
		connect(numSpokes*ring + numSpokes - 1,  ring*numSpokes);
	}	
	// at which point we have used numSpokes*numRings springs
	int used = numSpokes*numRings;
	// now do the rest of the radial spokes
	for(int ring = 0; ring < numRings-1; ring++){
		for(int spoke = 0; spoke < numSpokes; spoke++){
			connect(used,ring*numSpokes+spoke); // 
			connect(used,(ring+1)*numSpokes+spoke);  // going to next ring out
			springs[used].setRestLength(0.0);
			used++;
		}
	}
}

//--------------------------------------------------------------
void HoledSpiderMesh::constrain(double ax, double ay, constrainMode aMode){
	std::cout << "HOLED spider constrain\n";
	
	TwoVector diff;
	int i = 0;
	int startLump = 0;
	// check if lump is within grab range
	switch (aMode) {
		case CONSTRAIN_GRAB_REGION:
			for(i = 0; i<numLumps; i++){
				
				diff.setCoord(lumps[i].position.x - ax, lumps[i].position.y - ay);
				
				if (diff.norm() < GRAB_RANGE){
					lumps[i].constrain();
				}
			}
			break;
		case CONSTRAIN_EDGES:
		{
			// constrain outer ring
			startLump = numSpokes*(numRings-1);
			for(int l = startLump; l < startLump + numSpokes; l++){
				lumps[l].constrain();
			}
            
			break;
		}
		case CONSTRAIN_CORNERS:
			// constrain inner ring
			
			for(int l = startLump; l < startLump + numSpokes; l++){
				lumps[l].constrain();
			}
            
			break;
			
		default:
			break;
	}
}
//--------------------------------------------------------------

void HoledSpiderMesh::makeDefaultScanPath(){
	
	std::cout << "HOLED spider makeDefaultScanPath\n";
    
     // add a ring to the path
     int ringNo = floor(numRings/2.0);
     int springno = 0, lumpno = 0;
     for(int i = 0; i < numSpokes; i++){
         lumpno = numSpokes*ringNo + i;
         springno = numSpokes*ringNo + i;
         
         scanPath->addElement(&lumps[lumpno], &springs[springno]);
     }
     std::cout << "HOLED spider scan path created on ring " << ringNo << "\n";
     
    // add a spoke to the path
    /*
    int springno = 0, lumpno = 0;
    
    int springsinrings = numSpokes*numRings;
    for(int i = 0; i < numRings-1; i++){
        lumpno = numSpokes*i; // zero, numspokes etc
        springno = springsinrings + numSpokes*i; // starts from total rings, increments by numspokes etc
        scanPath->addElement(&lumps[lumpno], &springs[springno]);
    } 
     */
}

//--------------------------------------------------------------
/****************************************/
//		Holed Spider Cross   Mesh            */
/****************************************/
//--------------------------------------------------------------
SpiderCrossMesh::SpiderCrossMesh(int aNumSpokes,int aNumRings) : Mesh(){
	numSpokes = aNumSpokes;
	numRings = aNumRings;
    radial = true;
	
	makeComponents(numSpokes, numRings);
	
	setLumpPositions();
	
	makeConnections();
    
	makeDefaultScanPath();
    
    constrain(0.0,0.0,CONSTRAIN_EDGES);
    constrain(0.0,0.0,CONSTRAIN_CORNERS);
	
	std::cout << "constructed SpiderCrossMesh mesh\n";
}

void SpiderCrossMesh::makeComponents(int aDimension1, int aDimension2){
    // make componets creates the correct number of lumps and springs according to dimensions and mesh type
	std::cout << "SpiderCrossMesh spider makeComponents\n";
	numLumps = numSpokes * numRings; 
                // rings                // spokes  & diags
	numSprings = numSpokes * numRings + 3 * numSpokes * (numRings - 1);
	
	lumps = new Lump[numLumps];
	std::cout << "made " << numLumps << " lumps\n";
	springs = new Spring[numSprings];
	std::cout << "made " << numSprings << " springs\n";
	
    
}
//--------------------------------------------------------------
void SpiderCrossMesh::setLumpPositions(){
	std::cout << "SpiderCrossMesh spider setLumpPositions\n";
	// here we're assuming that zero spoke is to right
	// work out sines / cosines for each spoke
	int l = 0;
	double hyplen,xpos,ypos;
	
	double * cosines = new double[numSpokes];
	double * sines = new double[numSpokes];
	double angle = 2 * M_PI / numSpokes;
	
	double ringSpacing = 1.0/(2*numRings);
	
	for(int spoke = 0; spoke < numSpokes; spoke++){
		cosines[spoke] = cos(angle*spoke);
		sines[spoke] = sin(angle*spoke);
	}
    
	for(int ring = 0; ring < numRings; ring++){
		for(int spoke = 0; spoke < numSpokes; spoke++){
			hyplen = ringSpacing * (ring+1);
			xpos = hyplen * cosines[spoke] + 0.5;
			ypos = hyplen*sines[spoke] + 0.5;
			lumps[l].setPosition(xpos,ypos);
			l++;
			
		}
	}
	delete [] cosines;
	delete [] sines;
    
}
//--------------------------------------------------------------
void SpiderCrossMesh::makeConnections(){

	std::cout << "SpiderCrossMesh spider make connections\n";
    
	// UNLIKE a spider we're going to make rings first, to make indexing the scan path easier...
    
	for(int ring = 0; ring < numRings; ring++){
		for(int spoke = 0; spoke < numSpokes-1; spoke++){
			//		spring ,				lump
			connect(numSpokes*ring + spoke, ring*numSpokes + spoke + 0);
			connect(numSpokes*ring + spoke, ring*numSpokes + spoke + 1);
		}
		// remember the last one on the ring goes back to first
		//		spring ,						lump
		connect(numSpokes*ring + numSpokes - 1 , ring*numSpokes + numSpokes - 1);
		connect(numSpokes*ring + numSpokes - 1,  ring*numSpokes);
	}	
	// at which point we have used numSpokes*numRings springs
	int used = numSpokes*numRings;
    
	// now do radial spokes
	for(int ring = 0; ring < numRings-1; ring++){
		for(int spoke = 0; spoke < numSpokes; spoke++){
			connect(used,ring*numSpokes+spoke); // 
			connect(used,(ring+1)*numSpokes+spoke);  // going to next ring out
			springs[used].setRestLength(0.0);
			used++;
		}
	}
    // now do diag /
    for(int ring = 0; ring < numRings-1; ring++){
		for(int spoke = 0; spoke < numSpokes-1; spoke++){
			connect(used,ring*numSpokes+spoke,(ring+1)*numSpokes+spoke+1); // 
			springs[used].setRestLength(0.0);
			used++;
		}
        connect(used,ring*numSpokes+numSpokes-1,(ring+1)*numSpokes); // wrap to first 
        springs[used].setRestLength(0.0);
        used++;
	}
    
    // now do \ diag
    for(int ring = 0; ring < numRings-1; ring++){
        connect(used,ring*numSpokes,(ring+1)*numSpokes+numSpokes-1); // wrap first to last 
        springs[used].setRestLength(0.0);
        used++;
		for(int spoke = 1; spoke < numSpokes; spoke++){
			connect(used,ring*numSpokes+spoke,(ring+1)*numSpokes+spoke-1); // 
			springs[used].setRestLength(0.0);
			used++;
		}

	}
    std::cout << "used: " << used << " numSprings: " << numSprings << std::endl;
    
}

//--------------------------------------------------------------
void SpiderCrossMesh::constrain(double ax, double ay, constrainMode aMode){
	std::cout << "SpiderCrossMesh spider constrain\n";
	
	TwoVector diff;
	int i = 0;
	int startLump = 0;
	// check if lump is within grab range
	switch (aMode) {
		case CONSTRAIN_GRAB_REGION:
			for(i = 0; i<numLumps; i++){
				
				diff.setCoord(lumps[i].position.x - ax, lumps[i].position.y - ay);
				
				if (diff.norm() < GRAB_RANGE){
					lumps[i].constrain();
				}
			}
			break;
		case CONSTRAIN_EDGES:
		{
			// constrain outer ring
			startLump = numSpokes*(numRings-1);
			for(int l = startLump; l < startLump + numSpokes; l++){
				lumps[l].constrain();
			}
            
			break;
		}
		case CONSTRAIN_CORNERS:
			// constrain inner ring
			
			for(int l = startLump; l < startLump + numSpokes; l++){
				lumps[l].constrain();
			}
            
			break;
			
		default:
			break;
	}
}
//--------------------------------------------------------------

void SpiderCrossMesh::makeDefaultScanPath(){
	
	std::cout << "SpiderCrossMesh spider makeDefaultScanPath\n";
    
    // add a ring to the path
    int ringNo = floor(numRings/2.0);
    int springno = 0, lumpno = 0;
    for(int i = 0; i < numSpokes; i++){
        lumpno = numSpokes*ringNo + i;
        springno = numSpokes*ringNo + i;
        scanPath->addElement(&lumps[lumpno], &springs[springno]);
    }
    std::cout << "SpiderCrossMesh spider scan path created on ring " << ringNo << "\n";
    
    // add a spoke to the path
    /*
     int springno = 0, lumpno = 0;
     
     int springsinrings = numSpokes*numRings;
     for(int i = 0; i < numRings-1; i++){
     lumpno = numSpokes*i; // zero, numspokes etc
     springno = springsinrings + numSpokes*i; // starts from total rings, increments by numspokes etc
     scanPath->addElement(&lumps[lumpno], &springs[springno]);
     } 
     */
}

/****************************************/
//		Line    Mesh					*/
/****************************************/

LineMesh::LineMesh(int aNumSegments) : Mesh(){
	numSegments = aNumSegments;
    radial = false;
    
	makeComponents(aNumSegments, 0);
	
	setLumpPositions();
	
	makeConnections();
    
	makeDefaultScanPath();
    
    constrain(0.0,0.0,CONSTRAIN_EDGES);
    
    setPropagationSpeed(0.6);
	
	std::cout << "constructed LINE mesh\n";
	
}
//--------------------------------------------------------------
void LineMesh::makeComponents(int aDimension1, int aDimension2){
    // make componets creates the correct number of lumps and springs according to dimensions and mesh type
	std::cout << "Line class makeComponents\n";
    // 
    numLumps = numSegments+1;
    numSprings = numSegments;
    lumps = new Lump[numLumps];
	springs = new Spring[numSprings];
    
}
//--------------------------------------------------------------
void LineMesh::setLumpPositions(){
	std::cout << "LineMesh class setLumpPositions\n";
    // 
    double hspacing = (1.0 - 0.05)/numSegments;
    for(int i = 0; i < numLumps; i++){
        lumps[i].setPosition(0.025+hspacing*i, 0.5);
    }
}
//--------------------------------------------------------------
void LineMesh::makeConnections(){
    // connects the lmps and springs. the spring needs to know which lumps are at each end
    // and lumps need to know which springs attached
    // calls connect method that does this
	std::cout << "LineMesh class make connections\n";
    for(int i = 0; i < numSegments; i++){
        connect(i,i);
        connect(i,i+1);
    }
}

//--------------------------------------------------------------
void LineMesh::constrain(double ax, double ay, constrainMode aMode){
	std::cout << "LineMesh class constrain\n";
    if (aMode == CONSTRAIN_EDGES || aMode == CONSTRAIN_CORNERS){
        lumps[0].constrain();
        lumps[numLumps-1].constrain();
    }else if (aMode == CONSTRAIN_GRAB_REGION){
        TwoVector diff;
        for(int i = 0; i<numLumps; i++){
            
            diff.setCoord(lumps[i].position.x - ax, lumps[i].position.y - ay);
            
            if (diff.norm() < GRAB_RANGE){
                lumps[i].constrain();
            }
        }
    }
}
//--------------------------------------------------------------
//--------------------------------------------------------------
void LineMesh::makeDefaultScanPath(){
	
	std::cout << "LineMesh class makeDefaultScanPath\n";
    for(int i = 0; i < numSegments; i++){
        scanPath->addElement(&lumps[i], &springs[i]);
    }
}
//--------------------------------------------------------------
/****************************************/
//		GROUNDED LINE  Mesh					*/
/****************************************/

GroundedLineMesh::GroundedLineMesh(int aNumSegments) : Mesh(){
	numSegments = aNumSegments;
    radial = false;
	makeComponents(aNumSegments, 0);
	
	setLumpPositions();
	
	makeConnections();
    
	makeDefaultScanPath();
    
    constrain(0.0,0.0,CONSTRAIN_ALL_X);
    
    setPropagationSpeed(0.6);
	
	std::cout << "constructed GROUNDED LINE mesh\n";
	
}
//--------------------------------------------------------------
void GroundedLineMesh::makeComponents(int aDimension1, int aDimension2){
    // make componets creates the correct number of lumps and springs according to dimensions and mesh type
    
    // THIS IS CRAP should be able to just create new ones and attach them...?
	std::cout << "Line class makeComponents\n";
    // 
    numLumps = (numSegments+1)*2;
    numSprings = numSegments*2;
    lumps = new Lump[numLumps];
	springs = new Spring[numSprings];
    
}
//--------------------------------------------------------------
void GroundedLineMesh::setLumpPositions(){
	std::cout << "LineMesh class setLumpPositions\n";
    // 

    double hspacing = (1.0 - 0.05)/numSegments;
    for(int i = 0; i < (numSegments+1); i++){
        // moving ones
        lumps[i].setPosition(0.025+hspacing*i, 0.5);
        //grounded ones
        lumps[i + numSegments+1].setPosition(0.025+hspacing*i,0.5*cos(i*hspacing*M_PI) + 0.5 );
    }
    


}
//--------------------------------------------------------------
void GroundedLineMesh::makeConnections(){
    // connects the lmps and springs. the spring needs to know which lumps are at each end
    // and lumps need to know which springs attached
    // calls connect method that does this
	std::cout << "LineMesh class make connections\n";
    for(int i = 0; i < numSegments; i++){
        connect(i,i,i+1);
        connect(i + numSegments,i , i + numSegments+1);

    }

    setRestLength();
}

//--------------------------------------------------------------
void GroundedLineMesh::constrain(double ax, double ay, constrainMode aMode){
	std::cout << "LineMesh class constrain\n";
    if (aMode == CONSTRAIN_EDGES || aMode == CONSTRAIN_CORNERS){
        lumps[0].constrain();
        lumps[numLumps-1].constrain();
        
        for(int i = numSegments; i < numLumps; i++){
            lumps[i].constrain();
        }
    }

    if (aMode == CONSTRAIN_GRAB_REGION){
        TwoVector diff;
        for(int i = 0; i<numLumps; i++){
            
            diff.setCoord(lumps[i].position.x - ax, lumps[i].position.y - ay);
            
            if (diff.norm() < GRAB_RANGE){
                lumps[i].constrain();
            }
        }
    }
    if (aMode == CONSTRAIN_ALL_X){

        
        for(int i = 0; i<numSegments; i++){

            lumps[i].constrain(Lump::CONSTRAIN_X);
        }
        for(int i = numSegments; i < numLumps; i++){
            lumps[i].constrain();
        }
        lumps[0].constrain();
        lumps[numLumps-1].constrain();
    }
        
}
//--------------------------------------------------------------
//--------------------------------------------------------------
void GroundedLineMesh::makeDefaultScanPath(){
	
	std::cout << "LineMesh class makeDefaultScanPath\n";
    for(int i = 0; i < numSegments; i++){
        scanPath->addElement(&lumps[i], &springs[i]);
    }
}
//--------------------------------------------------------------

/****************************************/
//		DROPLET    Mesh					*/
/****************************************/
// more of a balloon really - filled with compressible gas
DropletMesh::DropletMesh(int aNumSegments) : Mesh(){
    
    restArea = 1.0;
    radial = true;
	numSegments = aNumSegments;
    
	makeComponents(aNumSegments, 0);
	
	setLumpPositions();
	
	makeConnections();
    
	makeDefaultScanPath();
    
    //constrain(0.0,0.0,CONSTRAIN_EDGES);
    
    setPropagationSpeed(0.8);
    
    zeroRestLength();
    
    forces.pressureAmt = 0.4;
	
	std::cout << "constructed DropletMesh mesh\n";
	
}
//--------------------------------------------------------------
void DropletMesh::makeComponents(int aDimension1, int aDimension2){
    // make componets creates the correct number of lumps and springs according to dimensions and mesh type
	std::cout << "Line class makeComponents\n";
    // 
    numLumps = numSegments;
    numSprings = numSegments;
    lumps = new Lump[numLumps];
	springs = new PressureSpring[numSprings];
    
}
//--------------------------------------------------------------
void DropletMesh::setLumpPositions(){
	std::cout << "DropletMesh class setLumpPositions\n";
    // make a circle centred around 0.5,0.5 radius 0.4
	int l = 0;
	double hyplen,xpos,ypos;
	
	double * cosines = new double[numSegments];
	double * sines = new double[numSegments];
	double angle = 2 * M_PI / numSegments;
    
	for(int spoke = 0; spoke < numSegments; spoke++){
		cosines[spoke] = cos(angle*spoke);
		sines[spoke] = sin(angle*spoke);
	}
    
    hyplen = 0.2; // aka radius
    
    // 
	//lumps[0].setPosition(0.5,0.5);
    
    for(int spoke = 0; spoke < numSegments; spoke++){
        
        xpos = hyplen * cosines[spoke] + 0.5;
        ypos = hyplen*sines[spoke] + 0.5;
        lumps[l].setPosition(xpos,ypos);
        l++;
        
    }
    calculateCentre();
    restArea = calculateArea() * 3; // this is like the amount of air in there originally, inflate a bit !
    
	delete [] cosines;
	delete [] sines;
}
//--------------------------------------------------------------
void DropletMesh::makeConnections(){
    // connects the lmps and springs. the spring needs to know which lumps are at each end
    // and lumps need to know which springs attached
    // calls connect method that does this
	std::cout << "DropletMesh class make connections\n";
    for(int i = 0; i < numSegments-1; i++){
        connect(i,i,i+1);
    }
    // finally connect end spring to start lump
    connect(numSegments-1,numSegments-1);
    connect(numSegments-1,0);
}

//--------------------------------------------------------------
void DropletMesh::constrain(double ax, double ay, constrainMode aMode){
	std::cout << "DropletMesh class constrain\n";
    if (aMode == CONSTRAIN_EDGES || aMode == CONSTRAIN_CORNERS){
        lumps[0].constrain();
        lumps[numLumps-1].constrain();
    }  else  if (aMode == CONSTRAIN_GRAB_REGION){
        TwoVector diff;
        for(int i = 0; i<numLumps; i++){
            
            diff.setCoord(lumps[i].position.x - ax, lumps[i].position.y - ay);
            
            if (diff.norm() < GRAB_RANGE){
                lumps[i].constrain();
            }
        }
    }
}
//--------------------------------------------------------------
//--------------------------------------------------------------
void DropletMesh::makeDefaultScanPath(){
	
	std::cout << "DropletMesh class makeDefaultScanPath\n";
    for(int i = 0; i < numSegments; i++){
        scanPath->addElement(&lumps[i], &springs[i]);
    }
    std::cout << "DropletMesh class makeDefaultScanPath\n";
}
//--------------------------------------------------------------
double DropletMesh::calculateArea(){
    
    // how do we calculate area?? answer: average all radii !
    
    // calc average radius magnitude
    double radiusMag = 0.0, avRadiusMag = 0.0;
    for(int i = 0; i < numLumps; i++){
        TwoVector radius;
        radius.setCoord(centre.x - lumps[i].position.x, centre.y - lumps[i].position.y );
        radiusMag = radius.norm();
        avRadiusMag += radiusMag;
    }
    avRadiusMag /= numLumps;
    
    // calc area
    double area = 2 * M_PI * avRadiusMag*avRadiusMag;
    
    return area;
    
}

double DropletMesh::calculatePressure(){
    // work out pressure on the springs. 
    // this will be proportional to the difference in the rest area to the current area
    //this will exert a force proportional to length
    
    double da = restArea - calculateArea();
    // if current area is smaller this will create positive pressure
    forces.setPressure(da);
    return forces.pressure;
    
}
//--------------------------------------------------------------
void DropletMesh::update(){

    calculateCentre();
    calculatePressure();
    Mesh::update(); // schweet
    
}
//--------------------------------------------------------------

/****************************************/
//		TriangleMesh					*/
/****************************************/
//--------------------------------------------------------------
TriangleMesh::TriangleMesh(int height, int width): Mesh() {
	radial = false;
	makeComponents(height, width);
	
	setLumpPositions();
	
	makeConnections();
	
	makeDefaultScanPath();
    
    constrain(0,0,CONSTRAIN_EDGES);
    
    zeroRestLength();
	
	std::cout << "constructing TRIANGLE mesh\n";
}
//--------------------------------------------------------------
// make componets for tri
void TriangleMesh::makeComponents(int aheight, int awidth){
	height = aheight;
	width = awidth;
	numLumps = height * width;
                /* horz   ---            \ diag                  / diag              */
	numSprings = (width-1) * (height-1) +  width * (height - 1) + (width-1) * (height - 1);
	
	lumps = new Lump[numLumps];
	std::cout << "made " << numLumps << " lumps\n";
	springs = new Spring[numSprings];
	std::cout << "made " << numSprings << " springs\n";
}

//--------------------------------------------------------------
void TriangleMesh::setLumpPositions(){
	double vspacing = 1.0/(height-1);
	double hspacing = 1.0/(width - 0.5);
	int i =0;
	// set positions
	for(int row = 0; row < height; row++){ // go down column
		for(int col = 0;col < width; col++){ // go along row
            // if even row start from wall
            if(row%2){ // odd
                lumps[i].setPosition(hspacing*col + 0.5*hspacing,vspacing*row);
			}else{ // even
                lumps[i].setPosition(hspacing*col,vspacing*row);
            }
			i++;
		}
	}
}
//--------------------------------------------------------------
void TriangleMesh::makeConnections(){
	
	// could be cleverer in terms of numbering?
	int lumpnum = 0;
	int springnum = 0;
    
	// attach horizontal a
	for ( int row=1;row< height;row++){
		for(int col=0;col<width-1;col++){
			connect(springnum,lumpnum,lumpnum+1);
            lumpnum++;
            springnum++;
		}
        lumpnum++;
	}
	
    /* now do /\/\/\/\/\/\/\ */
	lumpnum = 0;
	for ( int row=0;row<height-1;row++){
        
        if(row%2 == 0){ 
            // even ie 0
            /* start with \  diag */
            for(int col=0;col<width-1;col++){
                /* \ */
                connect(springnum++, lumpnum, lumpnum+ width);
                lumpnum++;
                /* / */
                connect(springnum++, lumpnum, lumpnum+(width-1));
                
            }
            connect(springnum++, lumpnum, lumpnum+width);
            lumpnum++;
            
        }else{         // odd
            for(int col=0;col<width-1;col++){
                /* / */
                connect(springnum++, lumpnum, lumpnum+ width);
                /* \ */
                connect(springnum++, lumpnum, lumpnum+(width+1));
                lumpnum++;
            }
            // last r to l
            /* / */
            connect(springnum++, lumpnum, lumpnum+(width));
            lumpnum++;
            
        }
    }
    
}	



//--------------------------------------------------------------
void TriangleMesh::constrain(double ax, double ay, constrainMode aMode){
	// exactly the same as square
	TwoVector diff;
	int i = 0;
	
	// check if lump is within grab range
	switch (aMode) {
		case CONSTRAIN_GRAB_REGION:
			for(i = 0; i<numLumps; i++){
				
				diff.setCoord(lumps[i].position.x - ax, lumps[i].position.y - ay);
				
				if (diff.norm() < GRAB_RANGE){
					lumps[i].constrain();
				}
			}
			break;
		case CONSTRAIN_EDGES:
			i = 0;
			for(int row = 0; row < height; row++){ // go down column
				for(int col = 0;col < width; col++){ // go along row
					if(row == 0 || row == height-1 || col == 0 || col == width-1){
						lumps[i].constrain();
					}
					i++;
				}
			}	
			break;
		case CONSTRAIN_CORNERS:
			i = 0;
			for(int row = 0; row < height; row++){ // go down column
				for(int col = 0;col < width; col++){ // go along row
					if(   (row == 0 && col == 0)
					   || (row == height-1 && col == 0) 
					   || (row == 0 && col == width-1) 
					   || (row == height-1 && col == width-1)){
						lumps[i].constrain();
					}
					i++;
				}
			}
			break;
			
		default:
			break;
	}
	
}
//--------------------------------------------------------------
//--------------------------------------------------------------
void TriangleMesh::makeDefaultScanPath(){

    int vmarg = 0;
    int hmarg = 1;
    
    int lumpno = vmarg * width + hmarg; // top left corner
    int springno = vmarg * (width - 1) + hmarg;
    // do top horz
    for(int i = 0; i < width - 2*hmarg; i++){
        scanPath->addElement(&lumps[lumpno], &springs[springno]);
        lumpno++;
        springno++;
    }

    
}

