/*
 *  2dvector.cpp
 *  simplespring
 *
 *  Created by Robert Tubb on 01/06/2011.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include "2dvector.h"
#include <iostream>
#include <cmath>

TwoVector::TwoVector(){
	x = 0.0;
	y = 0.0;
	//cout << "def constr set vector to zeros" << endl;
}

TwoVector::TwoVector(double ax, double ay){
	x = ax;
	y = ay;
	//cout << "spec constr set vector to " << ax << "," << ay << endl;
}

double TwoVector::norm(){
	double norm;
    norm = std::sqrt(x * x + y * y);
	return norm;
	
}

void TwoVector::setCoord(double ax, double ay){
	x = ax;
	y = ay;

}

TwoVector TwoVector::minus(TwoVector otherPoint){
    TwoVector diff;
    diff.setCoord(otherPoint.x - x, otherPoint.y - y);
    return diff;
}

TwoVector TwoVector::operator-(TwoVector otherPoint){
    TwoVector diff;
    diff.setCoord(otherPoint.x - x, otherPoint.y - y);
    return diff;
}


double TwoVector::distanceTo(TwoVector otherPoint){
    TwoVector diff;
    diff.setCoord(otherPoint.x - x, otherPoint.y - y);
    return diff.norm();
}
