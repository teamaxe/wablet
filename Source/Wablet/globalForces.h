//
//  globalForces.h
//  wablet
//
#ifndef _GLOBALFORCES
#define _GLOBALFORCES

#include "2dvector.h"
#include <memory>

struct forceTouchPoint{
    double x, y;
    int tid;

    //forceTouchPoint(double ax,double ay,int atid): x(ax),y(ay),tid(atid){};
    
};

class GlobalForces{

public:
	GlobalForces();
    ~GlobalForces();

    enum excitationTypes {POSITION,VELOCITY};
    enum excitationShapes {NOISE,GAUSS,SINE};

    excitationTypes excitationType;
    excitationShapes excitationShape;
    
    bool gravityOn, forceTouchOn, pressureOn;
    // params for adjusting stuff
    double pressureAmt, gravityAmt, touchStrength, excitationStrength, homingAmt, avFilterAmt;
    int exciteShapeX, exciteShapeY;
    // general params
    double speedLimit, wallBounce;
    double delt; // time step between frames
    double volume;
    
    forceTouchPoint* forceTouchPoints;
    TwoVector grav;
    double pressure;
    
    int maxForcePoints;
    
    void update();
    void setPressure(double aP);
    void createForceTouchPoint(double ax,double ay, int touchId);
    void moveForceTouchPoint(double ax,double ay, int touchId);
    void removeForceTouchPoint(int touchId);
    TwoVector getAllForceAt(double ax, double ay);
private:
    
};


#endif
