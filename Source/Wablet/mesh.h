/*
 *  mesh.h
 *  springstructure
 *
 *  Created by Robert Tubb on 07/06/2011.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef _MESHH
#define _MESHH

#include "../../JuceLibraryCode/JuceHeader.h"

#include <iostream>
#include "lump.h"
#include "scanpath.h"
#include "globalForces.h"
#include "randomnumber.h"


class Mesh{
public:

    bool radial; // tells us if mesh is radial or not
	// toggles
	bool syrup;
    bool home;

    double propagationSpeed;
    
    double k,m,f; // for stability check and asym
	
	int numLumps;
	int numSprings;
    
	Lump *lumps;
	Spring *springs;
    ScanPath* scanPath;
    
    TwoVector centre;

	
	// modes
	enum constrainMode { CONSTRAIN_CORNERS, CONSTRAIN_EDGES,CONSTRAIN_EDGES_XY, CONSTRAIN_SINGLE_POINT, CONSTRAIN_SINGLE_POINT_X, CONSTRAIN_SINGLE_POINT_Y, CONSTRAIN_GRAB_REGION,CONSTRAIN_GRAB_REGION_X,CONSTRAIN_GRAB_REGION_Y, CONSTRAIN_ALL_X, CONSTRAIN_ALL_Y};
	double GRAB_RANGE;
	 
	

    // MEMBER FUNCTIONS
	Mesh();
	virtual ~Mesh();

    void paint (Graphics& g, int x, int y);
    void draw();
    
	// INTERACTIONS
    void resetAll();
    void resetPositions();
    void resetVelocities();
    void resetEverything();
    
     // excite via position, velocity force
    // using shapes : noise, hamming, sine, 
    void hit(double dax,double day,int velocity = 100, GlobalForces::excitationTypes aEType = GlobalForces::POSITION, 
             GlobalForces::excitationShapes aEShape = GlobalForces::NOISE); 
    void spatialHarmonic(int aharm1 = 1, int aharm2 = 0);
    void damp();
    void forceField(double dax,double day,double strength);
    void averagingFilter(double amt);
    
	void grab(double x, double y,  int touchID); 
	void drag(double ax,double ay, int touchID);
	void unGrab(int touchID);

	void grabLump(int index, int touchID);
	void dragDelta(double ax, double ay, int touchID);

    
	virtual void constrain(double x, double y, constrainMode aMode);	// diff meshes will have diff edges
	void unconstrain();
    void unconstrain(double x, double y, constrainMode aMode);

    
    // PARAMETER SETTINGS
    void toggleSyrup();
	void toggleSyrup(bool on);
    void toggleSpringForces();
	void toggleSpringForces(bool on);
    void toggleGravity();
    void toggleGravity(bool on);
    void toggleHome(bool on);
    
	void setRestLength();
    void zeroRestLength();
    
    void setPropagationSpeed(double aSpeed); // sets k and m to give a certain speed
    void decreasePropagationSpeed();
    void increasePropagationSpeed();
    void setSpringConstant(double aK);
    void setMass(double aM);
    void setFriction(double aF);
    
    // create varying constants
    void setSpringConstantAsym(double aAsym);
    void setMassAsym(double aAsym);
    void setFrictionAsym(double aAsym);	
    void setZeroAudioLine();

    // only for mouse cursor
	void checkHover(double x, double y);

//	SCANPATH STUFF
	void clearScanPath();
    void disconnectDraw(); // enables you to draw scanpths anywhere
	int inscribeScanPath(double ax, double ay);
    // called by scanpath.update wavetables
	double getNextSample();
    
    virtual void update();

	GlobalForces& getForces() { return forces; }

protected:
	TwoVector* avLumpPos{nullptr};
	double prev;

	GlobalForces forces;
    int prevLump;
    
	// specific mesh shapes override these:

	virtual void makeComponents(int adimension1, int adimension2);
	virtual void setLumpPositions();
	virtual void makeConnections();
	virtual void makeDefaultScanPath();

    // UTILS    
    TwoVector calculateCentre();
	void connect(int springnum,int lumpnum);
    void connect(int springnum,int lumpnum,int alumpnum2);
	int getNearestLump(double ax,double ay);
    
    
private:
    RandomNumber randomNumber;
	
};
//---------------------------------------------------------
class SpiderMesh : public Mesh{
public:
	int numSpokes, numRings;
	
	SpiderMesh(int aNumSpokes,int aNumRings);
	
	virtual void constrain(double x, double y, constrainMode aMode);
	virtual void makeComponents(int adimension1, int adimension2);
	virtual void setLumpPositions();
	virtual void makeConnections();
	virtual void makeDefaultScanPath();
};
//---------------------------------------------------------
class HoledSpiderMesh : public Mesh{
public:
    int numSpokes, numRings;
	
    HoledSpiderMesh(int aNumSpokes,int aNumRings);
	void constrain(double x, double y, constrainMode aMode);
	void makeComponents(int adimension1, int adimension2);
	void setLumpPositions();
	void makeConnections();
	void makeDefaultScanPath();
};
//---------------------------------------------------------
class SpiderCrossMesh : public Mesh{
public:
	int numSpokes, numRings;
	
	SpiderCrossMesh(int aNumSpokes,int aNumRings);
	
	void constrain(double x, double y, constrainMode aMode);
	void makeComponents(int adimension1, int adimension2); // override
	void setLumpPositions(); // same
	void makeConnections(); // overrride
	void makeDefaultScanPath(); // same
};
//---------------------------------------------------------
// square grid with all diagonals connected
class SquareCrossMesh : public Mesh{
public:
	// square specific
	int height;
	int width;
	
	SquareCrossMesh(int height, int width);	
    ~SquareCrossMesh();
	void constrain(double x, double y, constrainMode aMode);
	 void makeComponents(int adimension1, int adimension2);
	 void setLumpPositions();
	 void makeConnections();
	 void makeDefaultScanPath();
};
//---------------------------------------------------------
// simple 1d line
class LineMesh : public Mesh{
public:
	// square specific
	int numSegments;
	
	LineMesh(int aNumSegments);	
	void constrain(double x, double y, constrainMode aMode);
    void makeComponents(int adimension1, int adimension2);
    void setLumpPositions();
    void makeConnections();
    void makeDefaultScanPath();
};
//---------------------------------------------------------
// simple 1d line attached to 'ground' (constrained lumps) by other springs...
class GroundedLineMesh : public Mesh{
public:
    int numSegments;
	
	GroundedLineMesh(int aNumSegments);	
	void constrain(double x, double y, constrainMode aMode);
    void makeComponents(int adimension1, int adimension2);
    void setLumpPositions();
    void makeConnections();
    void makeDefaultScanPath();
};

//----------------------------------------------------------
//---------------------------------------------------------
// circular spring with internal pressure
class DropletMesh : public Mesh{
public:
	// specific
	int numSegments;
    double restArea;
	
	DropletMesh(int aNumSegments);	
	void constrain(double x, double y, constrainMode aMode);
    void makeComponents(int adimension1, int adimension2);
    void setLumpPositions();
    void makeConnections();
    void makeDefaultScanPath();
    double calculatePressure();
    double calculateArea();
    void update();
};
//---------------------------------------------------------
// trangular mesh
class TriangleMesh : public Mesh{
public:
	// square specific
	int height;
	int width;
	
	TriangleMesh(int height, int width);	
	void constrain(double x, double y, constrainMode aMode);
    void makeComponents(int adimension1, int adimension2);
    void setLumpPositions();
    void makeConnections();
    void makeDefaultScanPath();
};
//----------------------------------------------------------

#endif
