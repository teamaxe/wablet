/*
  ==============================================================================

    randomnumber.h
    Created: 24 Nov 2020 3:08:08pm
    Author:  Tom Mitchell

  ==============================================================================
*/

#pragma once

#include <random>
#include <iostream>

class RandomNumber
{
public:
    RandomNumber() : dis (-1.0, 1.0)
    {
        std::random_device rd;  //Will be used to obtain a seed for the random number engine
        gen = std::mt19937 (rd()); //Standard mersenne_twister_engine seeded with rd()

    }
    double get() {return dis (gen);}
private:
    std::mt19937 gen; //Standard mersenne_twister_engine seeded with rd()
    std::uniform_real_distribution<> dis;
};
