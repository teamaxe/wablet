/*
  ==============================================================================

    MeshComponent.h
    Created: 22 Aug 2019 1:06:52pm
    Author:  tj3-mitchell

  ==============================================================================
*/

#include "../../../JuceLibraryCode/JuceHeader.h"
#include "../mesh.h"
#include "../globals.h"

#pragma once

class MeshComponent :   public Component,
                        public Timer
{
public:
    MeshComponent()
    {
 
    }
    ~MeshComponent() {}
    
    void setMesh (std::shared_ptr<Mesh> m)
    {
        mesh = m;
    }
    
    void setTouchMode (TouchMode newTouchMode) {touchMode = newTouchMode;}

	void updateMesh(bool shouldShow)
	{
		shouldShow ? startTimerHz (50) : stopTimer();
	}
    
    void paint (Graphics& g) override
    {
        if (mesh != nullptr)
        {
            mesh->paint (g, getWidth(), getHeight());
        }
    }
    
    void resized() override
    {

        
    }
    
    void mouseDown (const MouseEvent& event) override
    {
        if (mesh != nullptr)
        {
            auto dax = (double(event.x) - BORDER_SIZE)/getHeight();
            auto day = double(event.y)/getHeight();
			if (touchMode == Grab)
				mesh->grab(dax, day, 1);
			else if (touchMode == ForceField)
				mesh->getForces().createForceTouchPoint(dax, day, 1);
			else if (touchMode == SpatialHarmonic)
				mesh->spatialHarmonic(1, 0);
        }
    }

    void mouseDrag (const MouseEvent& event) override
    {
        if (mesh != nullptr)
        {
            auto dax = (double(event.x) - BORDER_SIZE)/getHeight();
            auto day = double(event.y)/getHeight();
			if (touchMode == Grab)
				mesh->drag(dax, day, 1);
			else if (touchMode == ForceField)
				mesh->getForces().moveForceTouchPoint(dax, day, 1);
        }
    }

    void mouseUp (const MouseEvent& /*event*/) override
    {
        if (mesh != nullptr)
        {
			if (touchMode == Grab)
				mesh->unGrab(1);
			else if (touchMode == ForceField)
				mesh->getForces().removeForceTouchPoint(1);
        }
    }
    
    void timerCallback() override
    {
        repaint();
    }
    
private:
    std::shared_ptr<Mesh> mesh;
    TouchMode touchMode {Grab};
};
