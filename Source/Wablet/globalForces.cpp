//
//  globalForces.cpp
//  wablet
//
//  Created by Robert Tubb on 18/07/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include "globalForces.h"
#include "globals.h"

//-----------------------------------------------------------
GlobalForces::GlobalForces(): grav(0.0,0.0){
    //construct
    gravityOn = true;
    
    speedLimit = 100.0;
    wallBounce = 0.5;
    
    pressureAmt = 0.0;
    gravityAmt = 0.2;
    touchStrength = 1.0;
    homingAmt = 0.0;
    avFilterAmt = 0.0;
    
    pressure = 0.0;
    volume = 1.0;
    
    maxForcePoints = 11;
    excitationType = POSITION;
    excitationShape = NOISE;

	excitationStrength = 100.0;
    
    forceTouchPoints = new forceTouchPoint[maxForcePoints];
    for(int i = 0; i< 11; i++)
	{
        forceTouchPoints[i].x = 0.0;
        forceTouchPoints[i].y = 0.0;
        forceTouchPoints[i].tid = -1;
    }
    
    
}

//-----------------------------------------------------------
GlobalForces::~GlobalForces(){
    //destruct
    
}
void GlobalForces::update(){
    // gravity
//    grav.setCoord(ofxAccelerometer.getForce().y * 0.015 * gravityAmt, ofxAccelerometer.getForce().x * 0.015 * gravityAmt);
    // time step
//#warning set framerate here
    delt = 1/60.0;
    
}
//-----------------------------------------------------------
// this is actually only called from dropletmesh.update
void GlobalForces::setPressure(double aP){
    
    pressure = pressureAmt*aP;
}
//-----------------------------------------------------------
void GlobalForces::createForceTouchPoint(double ax,double ay, int touchId){

    forceTouchPoints[touchId].x = ax;
    forceTouchPoints[touchId].y = ay;
    forceTouchPoints[touchId].tid = touchId; 

    
    
}
//-----------------------------------------------------------
void GlobalForces::moveForceTouchPoint(double ax,double ay, int touchId){
    //
    for(int i = 0;i<maxForcePoints; i++){
        if(forceTouchPoints[touchId].tid == touchId){
            forceTouchPoints[touchId].x = ax;
            forceTouchPoints[touchId].y = ay;            
        }
    }
    
}
//-----------------------------------------------------------
void GlobalForces::removeForceTouchPoint(int touchId){
    //
    for(int i = 0;i<maxForcePoints; i++){
        if(forceTouchPoints[i].tid == touchId){
            forceTouchPoints[i].tid = -1;          
        }
    } 
}
//-----------------------------------------------------------
TwoVector GlobalForces::getAllForceAt(double ax, double ay){
    //this is called for every mass point so work out most stuff in update()
    TwoVector force(0.0,0.0);
    double fmag, r;
    if(gravityOn){
        
        force.x += grav.x;
        force.y += grav.y;
    }
    
    // now touch points
     for(int i = 0;i<maxForcePoints; i++){
         // work out distance to force point and then use 1/r^2 rule 
         if(forceTouchPoints[i].tid != -1){
             TwoVector diff(forceTouchPoints[i].x - ax, forceTouchPoints[i].y - ay );
             r = diff.norm();
             if (r < 0.03){ // try avoid very small distances giving ridiculous forces
                 fmag = 0.0;
             }else{
                 fmag = touchStrength*1/(1000*(r*r));
             } 
            force.x -= fmag*diff.x/r;
            force.y -= fmag*diff.y/r;
             
         }
     }
    
    return force;
}
//-----------------------------------------------------------
