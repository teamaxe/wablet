/*
 *  2dvector.h
 *  simplespring
 *
 *  Created by Robert Tubb on 01/06/2011.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _2DVECTORH
#define _2DVECTORH

class TwoVector{
public:
	double x, y;
	TwoVector();
	TwoVector(double ax, double ay);

// public methods	
	double norm();
	void setCoord(double ax, double ay);
	TwoVector minus(TwoVector otherPoint);
    TwoVector operator-(TwoVector otherPoint);
    double distanceTo(TwoVector otherPoint);
	

};

#endif // #ifndef _2DVECTORH