/*
 *  scanpath.h
 *  springstructure
 *
 *  Created by Robert Tubb on 09/06/2011.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef _SCANPATHH
#define _SCANPATHH
#include "lump.h"
#include "spring.h"
#include <vector>


class ScanPath{
private:
	int n{0};

	int numElements;
    int maxElements;
    
    bool audioAccessing;
    bool updateAccessing;

	//Spring ** springPath;
	// springpath - an array of pointers to the springs in the path
	// lump path - ditto should be same number 
	Lump ** lumpPath;
    Spring ** springPath;
    
    double * wavetableNew;
    double * wavetableOld;
    double * wavetableUpdate;

    double currentLength;
    
    double frameInterpolator;
    double framesPerSample;
    
    

    
public:
    enum scanModes {DISPLACEMENT,SPEED,SPRING_FORCE,YPOS};

    scanModes scanMode;
    double restLength;
    
	ScanPath();
	~ScanPath();
    
    
	void inscribe(double ax,double ay);
	void clear();
    
    void draw();
    void drawCubic();
	
    double getTotalLength();
    int howManyElements();
    

	void addSpring();
	void addLump();
    double getNextSample();
    double getNextSample(double aPhasor);
    double getNextSampleCubic(double aPhasor);
    // redone stuff
    void initWavetables();
    void addElement(Lump* aLump, Spring * aSpring);

    void updateWavetables();

};

#endif
