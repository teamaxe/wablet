/*
 *  lump.h
 *  simplespring
 *
 *  Created by Robert Tubb on 01/06/2011.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _LUMPH
#define _LUMPH
#include "../../JuceLibraryCode/JuceHeader.h"

#include "2dvector.h"
#include "spring.h"
#include "globalForces.h"


class Spring;
class Lump {
private:
	double mass, inverseMass;
	bool grabbed, highlighted;
    const int maxSprings;
	
	TwoVector velocity;
	TwoVector accel;
	double friction;

public:
    bool constrained;
	Spring** attachedSprings; // pointers to all attached springs
	int numAttachedSprings;
    
    enum ConstrainMode {NOT_CONSTRAINED,CONSTRAIN_X,CONSTRAIN_Y,CONSTRAIN_XY};
    ConstrainMode constrainMode;
	
    bool isInScanPath;

	TwoVector position;
    TwoVector previousPosition;
	double totalForceMag;
	double size;
    int grabID; // which touch is this lump grabbed by?
		
	Lump(); // default constructor
	Lump(double aMass,double aFriction, double positionX, double positionY);
	// also which spring is it attached to
    ~Lump();
	
	TwoVector applyForce (GlobalForces& forces);
    void averagingFilter(double amt);
    void homingFilter(double amt);
    TwoVector averageOfConnected();

    TwoVector zeroRefPos;
	void draw();
    void paint (Graphics& g, int x, int y);

	void attachSpring(Spring* aSpring);
	void setPosition(double ax, double ay);
    void setVelocity(double ax, double ay);
	void constrain();
    void constrain(ConstrainMode aconstrainMode);
	void unconstrain();
	
	bool isGrabbed();
	void grab(int aGrabID);
	void drag(double ax, double ay, int aGrabID);
	void dragDelta (double ax, double ay, int aGrabID);
	void unGrab();
	void highlight();
	void unhighlight();
	void setInvMass(double aInvMass);
	double getTotalForceMag();
    void setZeroRefPos();
    void setFriction(double aF);
    
    Spring * checkConnectedTo(Lump * otherLump);
    
    // interface to scan path
    double scanDisplacement();
    double scanRadialDisplacement();
    double scanLumpSpeed();
    double scanYPos();
     double scanXPos();
    void addToScanPath();
    void removeFromScanPath();
	 
};

#endif
