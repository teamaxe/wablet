/*
  ==============================================================================

    Globals.h
    Created: 22 Aug 2019 10:22:42am
    Author:  tj3-mitchell

  ==============================================================================
*/

#pragma once

#define SAMPLE_RATE 44100.0
#define NUM_BUTTONS 12
#define BORDER_SIZE 128
#define BUTTON_SIZE 100

enum TouchMode
{
    Grab,
    ForceField,
    SpatialHarmonic
};

