//
//  dsptools.h
//  wablet
//
//  Created by Robert Tubb on 21/06/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#ifndef _DSPTOOLSH
#define _DSPTOOLSH


class DSPTools{
public:
    double ax[3];
    double by[3];

    
    double xv[3];
    double yv[3];
    
    DSPTools();
    double highpass1(double ax);
    double lowpass1(double ax);
    void getLPCoefficientsButterworth2Pole(const int samplerate, const double cutoff, double* const ax, double* const by);
    void getHPCoefficientsButterworth2Pole(double* const ax, double* const by);
    double butter(double sample);
    
};



#endif