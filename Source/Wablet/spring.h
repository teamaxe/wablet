/*
 *  spring.h
 *  simplespring
 *
 *  Created by Robert Tubb on 02/06/2011.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */



#ifndef _SPRINGH
#define _SPRINGH

#include "../../JuceLibraryCode/JuceHeader.h"
#include "2dvector.h"
//#include "ofMain.h"

class Lump;

class Spring{
protected:
	double restLength;
	double springConst;
	
	double maxForce{ 0.0 };
	double minForce{ 0.0 };
    
    double csquared;
	
	TwoVector startPoint;
	TwoVector endPoint;
	
	TwoVector force;
	Lump * startLumpPtr;
	Lump * endLumpPtr;
	
	bool isInScanPath;

	
public:
	static bool forcesOn;
	
	Spring();
    virtual ~Spring();
	Spring(double aStartx, double aStarty, double aEndx, double aEndy, double aK);
	void updateEndPoints();
	void attachLump(Lump * aLump);
    
    void calculateForce();
    virtual TwoVector getForce(Lump * aLump);

	void draw();
    void paint (Graphics& g, int x, int y);
    
	void setRestLength();
	void setRestLength(double aLength);
    void setSpringConstant(double aK);
    double getLength();

	void addToScanPath();
    void removeFromScanPath();
    Lump * getLumpOnOtherEnd(Lump * alump);
	//void checkStuff();
    
    // interface for scanner
	double getForceMag();
};

class PressureSpring : public Spring
{
public:
    PressureSpring();
    TwoVector getForce(Lump * aLump, double pressure);

};

#endif

	
