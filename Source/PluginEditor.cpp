/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
WabletAudioProcessorEditor::WabletAudioProcessorEditor (WabletAudioProcessor& p, AudioProcessorValueTreeState& vts)
    : AudioProcessorEditor (&p), processor (p), valueTreeState(vts)
{
    meshComponent.setMesh (processor.getMesh());
    addAndMakeVisible (meshComponent);

    addAndMakeVisible (speedLabel);
    speedLabel.attachToComponent (&speedSlider, true);
    speedSlider.onValueChange = [this](){processor.getMesh()->setPropagationSpeed (speedSlider.getValue());};
    speedAttachment.reset (new SliderAttachment (valueTreeState, WabletAudioProcessor::Ids::speed, speedSlider));
    addAndMakeVisible (speedSlider);

    addAndMakeVisible (smoothLabel);
    smoothLabel.attachToComponent (&smoothSlider, true);
    smoothSlider.onValueChange = [this]() {processor.getMesh()->getForces().avFilterAmt = smoothSlider.getValue();};
    smoothAttachment.reset (new SliderAttachment (valueTreeState, WabletAudioProcessor::Ids::smooth, smoothSlider));
    addAndMakeVisible (smoothSlider);

    addAndMakeVisible (springLabel);
    springLabel.attachToComponent (&springSlider, true);
    springSlider.onValueChange = [this]() {processor.getMesh()->setSpringConstant (springSlider.getValue()); };
    springAttachment.reset (new SliderAttachment (valueTreeState, WabletAudioProcessor::Ids::spring, springSlider));
    addAndMakeVisible (springSlider);

    addAndMakeVisible (massLabel);
    massLabel.attachToComponent (&massSlider, true);
    massSlider.onValueChange = [this]() {processor.getMesh()->setMass (massSlider.getValue()); };
    massAttachment.reset (new SliderAttachment (valueTreeState, WabletAudioProcessor::Ids::mass, massSlider));
    addAndMakeVisible (massSlider);

    addAndMakeVisible (frictionLabel);
    frictionLabel.attachToComponent (&frictionSlider, true);
    frictionSlider.onValueChange = [this]() {processor.getMesh()->setFriction (frictionSlider.getValue()); };
    frictionAttachment.reset (new SliderAttachment (valueTreeState, WabletAudioProcessor::Ids::friction, frictionSlider));
    addAndMakeVisible (frictionSlider);
    
    addAndMakeVisible (velocityLabel);
    velocityLabel.attachToComponent (&velocitySlider, true);
    velocityAttachment.reset (new SliderAttachment (valueTreeState, WabletAudioProcessor::Ids::velocity, velocitySlider));
    addAndMakeVisible (velocitySlider);
    
    stillButton.setClickingTogglesState (true);
    stillButton.onClick = [this](){processor.getMesh()->getForces().homingAmt = stillButton.getToggleState() ? 0.15 : 0.0;};
    stillAttachment.reset (new ButtonAttachment (valueTreeState, WabletAudioProcessor::Ids::still, stillButton));
    addAndMakeVisible (stillButton);
    
    driftButton.setClickingTogglesState (true);
    driftButton.onClick = [this](){processor.getMesh()->toggleSpringForces(! driftButton.getToggleState());};
    driftAttachment.reset (new ButtonAttachment (valueTreeState, WabletAudioProcessor::Ids::drift, driftButton));
    addAndMakeVisible (driftButton);
    
    gravityButton.setClickingTogglesState (true);
    gravityButton.onClick = [this]()
    {
        if (gravityButton.getToggleState())
        {
            processor.getMesh()->getForces().gravityAmt = 4.0;
            processor.getMesh()->toggleGravity (gravityButton.getToggleState());
        }
        else
        {
            processor.getMesh()->getForces().gravityAmt = 0.0;
            processor.getMesh()->toggleGravity (gravityButton.getToggleState());
        }
        
    };
    addAndMakeVisible (gravityButton);
    
    freeButton.setClickingTogglesState (true);
    freeButton.onClick = [this](){freeButton.getToggleState() ? processor.getMesh()->unconstrain() : processor.getMesh()->constrain(.0,.0,Mesh::CONSTRAIN_EDGES);};
    addAndMakeVisible (freeButton);

    visualsButton.setClickingTogglesState (true);
    visualsButton.onClick = [this](){ meshComponent.updateMesh (visualsButton.getToggleState()); };
    addAndMakeVisible (visualsButton);

    //Right buttons
    
    grabButton.setRadioGroupId (1);
    grabButton.onClick = [this](){meshComponent.setTouchMode (Grab);};
    addAndMakeVisible (grabButton);
    
    forceButton.setRadioGroupId (1);
    forceButton.onClick = [this](){meshComponent.setTouchMode (ForceField);};
    addAndMakeVisible (forceButton);
    
    spatialButton.setRadioGroupId (1);
    spatialButton.onClick = [this](){meshComponent.setTouchMode (SpatialHarmonic);};
    addAndMakeVisible (spatialButton);
    
    addAndMakeVisible (stickButton);
    addAndMakeVisible (unstickButton);
    
    resetButton.onClick = [this](){processor.getMesh()->resetAll();};
    addAndMakeVisible (resetButton);

    //startTimerHz (20);

    setSize(800, 600);
}

WabletAudioProcessorEditor::~WabletAudioProcessorEditor()
{
}

//==============================================================================
void WabletAudioProcessorEditor::paint (juce::Graphics& g)
{
    g.fillAll (getLookAndFeel().findColour (juce::ResizableWindow::backgroundColourId));
}

void WabletAudioProcessorEditor::resized()
{
    const auto buttonWidth = 100;
    const auto buttonHeight = 20;
    meshComponent.setBounds (getLocalBounds());
    
    auto left = getLocalBounds();

    auto right = left.removeFromRight (buttonWidth);
    left = left.removeFromLeft (buttonWidth);
    
    speedSlider.setBounds       (left.removeFromTop (buttonHeight).withX (50));
    smoothSlider.setBounds      (left.removeFromTop (buttonHeight).withX(50));
    springSlider.setBounds      (left.removeFromTop(buttonHeight).withX(50));
    massSlider.setBounds        (left.removeFromTop(buttonHeight).withX(50));
    frictionSlider.setBounds    (left.removeFromTop(buttonHeight).withX(50));
    velocitySlider.setBounds    (left.removeFromTop(buttonHeight).withX(50));
    
    stillButton.setBounds       (left.removeFromTop(buttonHeight));
    gravityButton.setBounds (left.removeFromTop (buttonHeight));
    driftButton.setBounds   (left.removeFromTop(buttonHeight));
    freeButton.setBounds    (left.removeFromTop (buttonHeight));
    visualsButton.setBounds (left.removeFromTop (buttonHeight));
    
    grabButton.setBounds    (right.removeFromTop (buttonHeight));
    forceButton.setBounds   (right.removeFromTop (buttonHeight));
    spatialButton.setBounds (right.removeFromTop (buttonHeight));
    stickButton.setBounds   (right.removeFromTop (buttonHeight));
    unstickButton.setBounds (right.removeFromTop (buttonHeight));
    resetButton.setBounds   (right.removeFromTop (buttonHeight));
}
