/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "PluginProcessor.h"
#include "Wablet/ui/MeshComponent.h"

//==============================================================================
/**
*/
class WabletAudioProcessorEditor  : public juce::AudioProcessorEditor
{
public:
    WabletAudioProcessorEditor (WabletAudioProcessor&, AudioProcessorValueTreeState&);
    ~WabletAudioProcessorEditor() override;

    //==============================================================================
    void paint (juce::Graphics&) override;
    void resized() override;
    

private:
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    WabletAudioProcessor& processor;
    AudioProcessorValueTreeState& valueTreeState;
    
    typedef AudioProcessorValueTreeState::SliderAttachment SliderAttachment;
    typedef AudioProcessorValueTreeState::ButtonAttachment ButtonAttachment;
    
    MeshComponent meshComponent;

    TextButton stillButton              {WabletAudioProcessor::Ids::still};
    std::unique_ptr<ButtonAttachment>   stillAttachment;
    
    TextButton driftButton              {WabletAudioProcessor::Ids::drift};
    std::unique_ptr<ButtonAttachment>   driftAttachment;
    
    TextButton gravityButton            {"gravity"};
    
    TextButton freeButton               {"free"};
    std::unique_ptr<ButtonAttachment>   freeAttachment;
    
    Label speedLabel                    {"", WabletAudioProcessor::Ids::speed};
    Slider speedSlider                  {Slider::LinearBar, Slider::NoTextBox};
    std::unique_ptr<SliderAttachment>   speedAttachment;
    
    Label smoothLabel                   {"", WabletAudioProcessor::Ids::smooth};
    Slider smoothSlider                 {Slider::LinearBar, Slider::NoTextBox};
    std::unique_ptr<SliderAttachment>   smoothAttachment;
    
    Label springLabel                   { "", WabletAudioProcessor::Ids::spring };
    Slider springSlider                 { Slider::LinearBar, Slider::NoTextBox };
    std::unique_ptr<SliderAttachment>   springAttachment;
    
    Label massLabel                     { "", WabletAudioProcessor::Ids::mass };
    Slider massSlider                   { Slider::LinearBar, Slider::NoTextBox };
    std::unique_ptr<SliderAttachment>   massAttachment;
    
    Label frictionLabel                 { "", WabletAudioProcessor::Ids::friction };
    Slider frictionSlider               { Slider::LinearBar, Slider::NoTextBox };
    std::unique_ptr<SliderAttachment>   frictionAttachment;
    
    Label velocityLabel                 { "", WabletAudioProcessor::Ids::velocity };
    Slider velocitySlider               { Slider::LinearBar, Slider::NoTextBox };
    std::unique_ptr<SliderAttachment>   velocityAttachment;
    
    TextButton visualsButton    { "Animate Mesh" };
    
    //touchMode buttons
    TextButton grabButton       {"grab"};
    TextButton forceButton      {"force"};
    TextButton spatialButton    {"spatial"};
    TextButton stickButton      {"stick"};
    TextButton unstickButton    {"unstick"};
    TextButton resetButton      {"reset"};

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (WabletAudioProcessorEditor)
};
