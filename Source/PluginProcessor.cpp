/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"
#include "Wablet/globals.h"

const String WabletAudioProcessor::Ids::WABLET   ("WABLET");
const String WabletAudioProcessor::Ids::speed    ("speed");
const String WabletAudioProcessor::Ids::smooth   ("smooth");
const String WabletAudioProcessor::Ids::spring   ("spring");
const String WabletAudioProcessor::Ids::mass     ("mass");
const String WabletAudioProcessor::Ids::friction ("friction");
const String WabletAudioProcessor::Ids::velocity ("velocity");
const String WabletAudioProcessor::Ids::drift    ("drift");
const String WabletAudioProcessor::Ids::still    ("still");

//==============================================================================
WabletAudioProcessor::WabletAudioProcessor()
 :  parameters (*this, nullptr, "state",
               {std::make_unique<AudioParameterFloat> (Ids::speed,    "Speed",     NormalisableRange<float> (0.0f, 1.0f), 0.5f),
                std::make_unique<AudioParameterFloat> (Ids::smooth,   "Smoothing", NormalisableRange<float> (0.0f, 1.0f), 0.01f),
                std::make_unique<AudioParameterFloat> (Ids::spring,   "Spring",    NormalisableRange<float> (0.0f, 1.0f), 0.8f),
                std::make_unique<AudioParameterFloat> (Ids::mass,     "Mass",      NormalisableRange<float> (0.0f, 1.0f), 1.f),
                std::make_unique<AudioParameterFloat> (Ids::friction, "Friction",  NormalisableRange<float> (0.0f, 1.0f), 0.99991f),
                std::make_unique<AudioParameterFloat> (Ids::velocity, "Velocity",  NormalisableRange<float> (0.0f, 1.0f), 0.5f),
                std::make_unique<AudioParameterBool>  (Ids::drift,    "Drift",     false),
                std::make_unique<AudioParameterBool>  (Ids::still,    "Still",     false) })
{
    theMesh = std::make_shared<SpiderCrossMesh>(50,15);
    
    theMesh->setSpringConstant(0.8);
    theMesh->setMass(1);
    theMesh->setFriction(0.99991);
    
    theMesh->scanPath->scanMode = theMesh->scanPath->DISPLACEMENT;
    
    pitch = 60.0;
    phasorIncr = pitch/SAMPLE_RATE;
    
    auto& forces = theMesh->getForces();
    forces.gravityAmt = 0.0;
    forces.avFilterAmt = 0.01;
    
    velocitySensativityParameter = parameters.getRawParameterValue (Ids::velocity);
}

WabletAudioProcessor::~WabletAudioProcessor()
{
}

//==============================================================================
const juce::String WabletAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool WabletAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool WabletAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool WabletAudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double WabletAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int WabletAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int WabletAudioProcessor::getCurrentProgram()
{
    return 0;
}

void WabletAudioProcessor::setCurrentProgram (int index)
{
}

const juce::String WabletAudioProcessor::getProgramName (int index)
{
    return {};
}

void WabletAudioProcessor::changeProgramName (int index, const juce::String& newName)
{
}

//==============================================================================
void WabletAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    updateInterval = roundToInt (sampleRate / 60.0);
    updateCounter = 0;
}

void WabletAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool WabletAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    juce::ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    if (layouts.getMainOutputChannelSet() != juce::AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != juce::AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

void WabletAudioProcessor::processBlock (juce::AudioBuffer<float>& buffer, juce::MidiBuffer& midiMessages)
{
    juce::ScopedNoDenormals noDenormals;
    auto totalNumInputChannels  = getTotalNumInputChannels();
    auto totalNumOutputChannels = getTotalNumOutputChannels();
    
    const auto velocitySensitivity = velocitySensativityParameter->load();

    // In case we have more outputs than inputs, this code clears any output
    // channels that didn't contain input data, (because these aren't
    // guaranteed to be empty - they may contain garbage).
    // This is here to avoid people getting screaming feedback
    // when they first compile a plugin, but obviously you don't need to keep
    // this code if your algorithm always overwrites all the output channels.
    for (auto i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear (i, 0, buffer.getNumSamples());
    
    phasorIncr = pitch / SAMPLE_RATE;
    auto midiIterator = midiMessages.begin();
    bool noteReady = midiIterator != midiMessages.end();
    
    for (int sampleCounter = 0; sampleCounter < buffer.getNumSamples(); sampleCounter++)
     {
         if (updateCounter++ >= updateInterval)
         {
             updateCounter = 0;
             theMesh->update();
         }
         
         if (noteReady && sampleCounter == (*midiIterator).samplePosition)
         {
             auto midiMessage = (*midiIterator).getMessage();
             if (midiMessage.isNoteOn())
             {
                 pitch = MidiMessage::getMidiNoteInHertz (midiMessage.getNoteNumber());
                 theMesh->hit (0.5,0.5, midiMessage.getVelocity() * velocitySensitivity, GlobalForces::POSITION, GlobalForces::NOISE);
                 midiIterator++;
                 noteReady = midiIterator != midiMessages.end();
             }
         }
         
         float* outL = buffer.getWritePointer (0, sampleCounter);
         //float* outR = buffer.getWritePointer (1, sampleCounter);
         
         auto sample  = float(theMesh->scanPath->getNextSample (phasor));
         //sample  = float(theMesh->scanPath->getNextSample());
         // BASIC STRING sample = float(basicString->getNextSample(phasor));
         
         // TODO hipass to get rid of DC
         //sample = mydspTools.highpass1(sample);
         sample = (float)mydspTools.butter(sample);
         
         
         *outL /*= *outR */= sample * (float)theMesh->getForces().volume;
         
         phasor += phasorIncr;
         
         if(phasor >= 1.0)
         {
             phasor -= 1.0;
         }
     }
    
//    for (int channel = 0; channel < totalNumInputChannels; ++channel)
//    {
//        auto* channelData = buffer.getWritePointer (channel);
//
//        // ..do something to the data...
//    }
}

//==============================================================================
bool WabletAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

juce::AudioProcessorEditor* WabletAudioProcessor::createEditor()
{
    return new WabletAudioProcessorEditor (*this, parameters);
}

//==============================================================================
void WabletAudioProcessor::getStateInformation (juce::MemoryBlock& destData)
{
    auto state = parameters.copyState();
    std::unique_ptr<juce::XmlElement> xml (state.createXml());
    copyXmlToBinary (*xml, destData);
}

void WabletAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    std::unique_ptr<juce::XmlElement> xmlState (getXmlFromBinary (data, sizeInBytes));
 
        if (xmlState.get() != nullptr)
            if (xmlState->hasTagName (parameters.state.getType()))
                parameters.replaceState (juce::ValueTree::fromXml (*xmlState));
}

//==============================================================================
// This creates new instances of the plugin..
juce::AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new WabletAudioProcessor();
}
