/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "Wablet/mesh.h"
#include "Wablet/dsptools.h"

//==============================================================================
/**
*/
class WabletAudioProcessor  : public juce::AudioProcessor
{
public:
    //==============================================================================
    WabletAudioProcessor();
    ~WabletAudioProcessor() override;

    struct Ids
    {
        static const String WABLET;
        static const String speed;
        static const String smooth;
        static const String spring;
        static const String mass;
        static const String friction;
        static const String velocity;
        
        static const String drift;
        static const String still;
    };
    
    std::shared_ptr<Mesh> getMesh() {return theMesh;}

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

   #ifndef JucePlugin_PreferredChannelConfigurations
    bool isBusesLayoutSupported (const BusesLayout& layouts) const override;
   #endif

    void processBlock (juce::AudioBuffer<float>&, juce::MidiBuffer&) override;

    //==============================================================================
    juce::AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const juce::String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    bool isMidiEffect() const override;
    double getTailLengthSeconds() const override;

    //==============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const juce::String getProgramName (int index) override;
    void changeProgramName (int index, const juce::String& newName) override;

    //==============================================================================
    void getStateInformation (juce::MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;

private:
    // Our plug-in's current state
    AudioProcessorValueTreeState parameters;
    std::atomic<float>* velocitySensativityParameter = nullptr;
    
    std::shared_ptr<Mesh> theMesh;
    
    double pitch;
    double phasor{0.0};
    double phasorIncr;
    
    int updateInterval {44100};
    int updateCounter {0};
    
    DSPTools mydspTools;

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (WabletAudioProcessor)
};
